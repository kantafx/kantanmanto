package com.kantafx.order.services;

import com.kantafx.order.exception.ResourceNotFoundException;
import com.kantafx.order.models.Administrator;
import com.kantafx.order.models.Client;
import com.kantafx.order.payloads.AdminDetailsResponse;
import com.kantafx.order.payloads.ClientDetailsResponse;
import com.kantafx.order.payloads.LoginRequest;
import com.kantafx.order.repositories.AdministratorRepository;
import com.kantafx.order.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {

    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private ClientRepository clientRepository;

    public AdminDetailsResponse verifyAdministrator(LoginRequest request) throws ResourceNotFoundException {

        Optional<Administrator> administrator = administratorRepository.findByEmail(request.email());

        if(administrator.isPresent()
        &&administrator.get().getPassword().equals(request.password()))
        {
            return new AdminDetailsResponse(administrator.get().getEmail());
        }


        throw new ResourceNotFoundException("Wrong credentials");
    }


    public ClientDetailsResponse verifyClient(LoginRequest request) throws ResourceNotFoundException {

        Optional<Client> client = clientRepository.findByEmail(request.email());

        if(client.isPresent()
                &&client.get().getPassword().equals(request.password()))
        {
            return new ClientDetailsResponse(client.get().getName(), client.get().getClientId(), client.get().getEmail());
        }


        throw new ResourceNotFoundException("Wrong credentials");
    }


}
