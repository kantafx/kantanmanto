package com.kantafx.order.services;

import com.kantafx.order.RabbitConfig;
import com.kantafx.order.payloads.TimestampedOrderBook;
import com.kantafx.order.repositories.OrderBookCacheRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class OrderBookListener {
    @RabbitListener(queues = RabbitConfig.ORDER_BOOK_QUEUE)
    public void listener(TimestampedOrderBook payload) {
        OrderBookCacheRepository.populateOrderBooks(payload);
    }
}
