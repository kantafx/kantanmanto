package com.kantafx.order.services;

public class ExchangeWorker {
    private  static String concat = "<>";

    public static String buildKey(String id, String url){
        return id + concat + url;
    }

    public static String getId(String str){
        String []arr = str.split(concat);
        return arr[0];
    }

    public static String getUrl(String str){
        String []arr = str.split(concat);
        return arr[1];
    }
}
