package com.kantafx.order.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class MarketDataInfo {
    private double maxPriceShift;
    private double currentPrice;
}
