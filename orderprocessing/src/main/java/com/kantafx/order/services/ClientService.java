package com.kantafx.order.services;

import com.kantafx.order.exception.ResourceAlreadyExistsException;
import com.kantafx.order.exception.ResourceCouldNotBeCreatedException;
import com.kantafx.order.exception.ResourceNotFoundException;
import com.kantafx.order.exception.UnprocessableRequestException;
import com.kantafx.order.models.*;
import com.kantafx.order.payloads.*;
import com.kantafx.order.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;


    @Autowired
    private ExecutionRepository executionRepository;

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Autowired
    private ClientOrderRepository clientOrderRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
     private TickerRepository tickerRepository;

    @Autowired
    private OrderValidationService orderValidationService;

    @Autowired
    private OrderProcessingService orderProcessingService;

    @Autowired
    private TradeRepository tradeRepository;

    @Autowired
    private WebClientService webClientService;




    public ClientDetailsResponse saveClient(ClientRegistrationRequest request) throws Exception {
        //first check if client exists in db
        Optional<Client> findClientFromDB = clientRepository.findByEmail(request.email());

        if (findClientFromDB.isPresent())
        {
            throw new ResourceAlreadyExistsException("User email already exists!");
        }


        //if client not present then proceed with save.
        Client client = Client.builder()
                .name(request.name())
                .email(request.email())
                .password(request.password())
                .build();

        Client savedClient = clientRepository.save(client);
        if(Objects.nonNull(savedClient))
        {
            buildDefaultPortfolio(savedClient);

            return new ClientDetailsResponse(savedClient.getName(),
                    savedClient.getClientId(), savedClient.getEmail());
        }

        throw new ResourceCouldNotBeCreatedException("Failed to create client");
}

    public void buildDefaultPortfolio(Client savedClient) throws ResourceCouldNotBeCreatedException {
        Portfolio portfolio = Portfolio.builder()
                .portfolioName("Default Portfolio")
                .client(savedClient)
                .amount(100000d)
                .createdAt(Timestamp.from(Instant.now()))
                .stock(new ArrayList<>())
                .isDefault(true)
                .build();

        Portfolio defaultPortfolio =  portfolioRepository.save(portfolio);

        if(Objects.isNull(defaultPortfolio))
        {
            clientRepository.delete(savedClient);
            throw new ResourceCouldNotBeCreatedException("Failed to create client because default portfolio could not be created.");
        }
    }


    public void createPortfolio(NewPortfolioRequest request) throws Exception {
        Optional<Client> client = clientRepository.findByEmail(request.email());

        if(client.isEmpty())
        {
            throw new ResourceNotFoundException("Could not find client.");
        }

        Portfolio portfolio = Portfolio.builder()
                .portfolioName(request.portfolioName())
                .client(client.get())
                .amount(request.amount())
                .createdAt(Timestamp.from(Instant.now()))
                .isDefault(false)
                .stock(new ArrayList<>())
                .build();


        Optional<Portfolio> savedPortfolio = Optional.of(portfolioRepository.save(portfolio));

        if(savedPortfolio.isEmpty())
        {
            throw new ResourceCouldNotBeCreatedException("Failed to create portfolio");
        }
    }


    public List<Portfolio> fetchAllPortfoliosByClient(Long clientId) throws ResourceNotFoundException {
        Optional<Client> client = clientRepository.findById(clientId);


        if(client.isPresent())
        {
            List<Portfolio> portfolios = portfolioRepository.findAllByClientOrderByCreatedAtAsc(client.get());

            return portfolios;
        }

        throw new ResourceNotFoundException("Could not find requested client.");
    }

    public String createOrder(ClientOrderRequest request) throws Exception{
        if(request.quantity() <= 0)
        {
            throw new ResourceCouldNotBeCreatedException("Order quantity cannot be zero");
        }


        //get the ticker
        Optional<Ticker> ticker = tickerRepository.findByTickerLabel(request.tickerLable().toString());

        if(ticker.isEmpty()) throw new ResourceCouldNotBeCreatedException("Failed to create order, ticker does not exist");

        //get the portfolio
        Optional<Portfolio> portfolio = portfolioRepository.findById(request.portfolioId());

        if(portfolio.isEmpty()) throw new ResourceCouldNotBeCreatedException("Failed to create order, portfolio does not exist");

        //build the order from request
        ClientOrder order = ClientOrder.builder()
                .ticker(ticker.get())
                .price(request.price())
                .quantity(request.quantity())
                .side(request.side())
                .portfolio(portfolio.get())
                .orderType(request.orderType())
                .trades(new ArrayList<>())
                .createdAt(Timestamp.from(Instant.now()))
                .executedQuantity(0)
                .status(Status.CREATED)
                .build();


        //validate order
        List<MarketData> validExchanges = orderValidationService.getValidExchangesForOrder(order);

        if(validExchanges.size() == 0) throw new ResourceCouldNotBeCreatedException("Order unsuitable for available Exchanges");

        //Passed all validation. Processing should begin

        return orderProcessingService.processOrder(order, validExchanges);
    }


    public Stock createStock(StockRequest request) throws ResourceCouldNotBeCreatedException {
        Optional<Ticker> ticker = tickerRepository.findByTickerLabel(request.tickerLable().toString());
        if(ticker.isEmpty())
        {
            throw new ResourceCouldNotBeCreatedException("Invalid ticker!");
        }


        Optional<Portfolio> portfolio = portfolioRepository.findById(request.portfolioId());

        if(portfolio.isEmpty())
        {
            throw new ResourceCouldNotBeCreatedException("Invalid portfolio!");
        }

        Double value = request.price() * request.quantity();

        if(ticker.isPresent() && portfolio.isPresent()){
            Stock stock = Stock.builder()
                    .ticker(ticker.get())
                    .value(value)
                    .portfolio(portfolio.get())
                    .quantity(request.quantity())
                    .build();

            return stockRepository.save(stock);
        }


        throw new ResourceCouldNotBeCreatedException("An error occurred, could not create stock.");
    }

    public Trade cancelOrder(String exchangeOrderId) throws ResourceNotFoundException {

        Optional<Trade> savedTrade = tradeRepository.findById(exchangeOrderId);

        if(savedTrade.isPresent())
        {
            //query market data for endpoint of exchange
            String endpoint = queryMarketDataForExchangeEndpoint(savedTrade.get().getExchangeName());

            //send cancellation to exchange
            sendCancellationRequestToExchange(endpoint, exchangeOrderId);

            //set cancellation status on trade
            savedTrade.get().setStatus(Status.CANCELLED);

            //find out if all legs of the order have been cancelled and update status in the order table
            ClientOrder originalOrder = savedTrade.get().getOrder();
            checkIfAllTradesFromOrderCancelled(originalOrder);

            //update portfolio balance
            updatePortfolioBalanceOnCancellation(savedTrade.get());

            //set update time on order
            originalOrder.setUpdatedAt(Timestamp.from(Instant.now()));

            //save updates
            clientOrderRepository.save(originalOrder);
            return tradeRepository.save(savedTrade.get());
        }
        else
        {
            throw new ResourceNotFoundException("Invalid exchange order id");
        }
    }

    public String queryMarketDataForExchangeEndpoint(String exchangeName)
    {
        return webClientService.queryMarketDataForExchangeEndpoint(exchangeName);
    }

    public void sendCancellationRequestToExchange(String endpoint, String exchangeOrderId)
    {
        webClientService.sendCancellationRequestToExchange(endpoint, exchangeOrderId);
    }


    public void updatePortfolioBalanceOnCancellation(Trade savedTrade)
    {
        Portfolio portfolio = savedTrade.getOrder().getPortfolio();
        portfolio.setAmount(portfolio.getAmount() + savedTrade.getPrice()*savedTrade.getQuantity());
        portfolioRepository.save(portfolio);
    }


    public void checkIfAllTradesFromOrderCancelled(ClientOrder order)
    {
        List<Trade> tradesInOrder = order.getTrades();

        Boolean allOrdersCancelled = false;

        for(Trade trade: tradesInOrder)
        {
            if(trade.getStatus() == Status.CANCELLED)
            {
                allOrdersCancelled = true;
            }
            else
            {
                allOrdersCancelled = false;
            }
        }

        if(allOrdersCancelled)
        {
            order.setStatus(Status.CANCELLED);
        }
    };


    public Portfolio fetchClientPortfolioById(Long clientId, Long portfolioId) throws ResourceNotFoundException {
        Optional<Client> client = clientRepository.findById(clientId);

        if(client.isEmpty()) throw new ResourceNotFoundException("Client does not exist!");

        Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);

        if(portfolio.isPresent())
        {
            return portfolio.get();
        }

        throw new ResourceNotFoundException("Portfolio could not be found!.");
    }

    public List<FetchAllClientOrdersResponse> fetchAllOrdersByClient(Long clientId) throws ResourceNotFoundException {
        Optional<Client> client = clientRepository.findById(clientId);


        if(client.isPresent())
        {
            return clientOrderRepository.findAllByClientId(clientId);
        }

        throw new ResourceNotFoundException("Invalid Client");
    }


    public void closePortfolio(Long portfolioDeleting) throws Exception{
        Optional<Portfolio> closingPortfolio = portfolioRepository.findById(portfolioDeleting);

        if (closingPortfolio.isEmpty()){
            throw new ResourceNotFoundException("Invalid Closing Portfolio");
        }

        if(closingPortfolio.get().getIsDefault() == true){
            throw new UnprocessableRequestException("Cannot delete default portfolio");
        }


        Client client = closingPortfolio.get().getClient();
        Optional<Portfolio> defaultPortfolio = portfolioRepository.findByClientAndIsDefault(client, true);


        if (defaultPortfolio.isEmpty()){
            throw new ResourceNotFoundException("Default portfolio does not exist");
        }

        List<Stock> ownedStocks = stockRepository.findAllByPortfolio(closingPortfolio.get());

        if(ownedStocks.size() > 0){
            throw new UnprocessableRequestException("Portfolio must be emptied of all stocks owned");
        }

        // add amount to portfolio
        updateDefaultPortfolioAmountOnPortfolioClose(defaultPortfolio.get(), closingPortfolio.get());

        portfolioRepository.save(defaultPortfolio.get());


        //place all orders into default portfolio
        pointAllOrdersInClosedPortfolioToDefaultPortfolio(defaultPortfolio.get(), closingPortfolio.get());

        //delete portfolio
        portfolioRepository.delete(closingPortfolio.get());
    }

    public void updateDefaultPortfolioAmountOnPortfolioClose(Portfolio defaultPortfolio, Portfolio closingPortfolio)
    {
        defaultPortfolio.setAmount(defaultPortfolio.getAmount() + closingPortfolio.getAmount());
    }

    public void pointAllOrdersInClosedPortfolioToDefaultPortfolio(Portfolio defaultPortfolio, Portfolio closingPortfolio)
    {
        List<ClientOrder> allOrders = clientOrderRepository.findAllByPortfolio(closingPortfolio);

        allOrders.stream().forEach(order -> {
            order.setPortfolio(defaultPortfolio);
            order.setUpdatedAt(Timestamp.from(Instant.now()));
        });

        clientOrderRepository.saveAll(allOrders);
    }

    public List<Ticker> fetchAllTickers() {
        return tickerRepository.findAll();
    }

    public List<ClientOrder> fetchAllOrdersByPortfolio(Long portfolioId) throws ResourceNotFoundException {
        Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);

        if(portfolio.isEmpty())
        {
            throw new ResourceNotFoundException("Failed to load portfolio");
        }

        return clientOrderRepository.findAllByPortfolio(portfolio.get());
    }

    public List<FetchAllClientOrdersByPortfolioWherePendingResponse> fetchAllOrdersByPortfolioWhereOrderPending(Long portfolioId) throws ResourceNotFoundException {
        Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);

        if(portfolio.isEmpty())
        {
            throw new ResourceNotFoundException("Failed to load portfolio");
        }

        List<FetchAllClientOrdersByPortfolioWherePendingResponse> allOrdersFromRep = clientOrderRepository.findAllByPortfolioWhereStatusCreated(portfolio.get().getPortfolioId());


        return allOrdersFromRep;
    }



    public void trackOrder(ExecutionPayload payload)
    {
        if(payload.getExecutions().size() == 0) return;

        Optional<Trade> trade = tradeRepository.findById(payload.getOrderID());

        if(trade.isPresent())
        {
            ClientOrder order = trade.get().getOrder();

            //correct price on market orders
            if(order.getOrderType() ==OrderType.MARKET) trade.get().setPrice(payload.getCumulativePrice());


            //add to holdings
            //check if ticker exist
            Optional<Stock> possibleStock = stockRepository.findByTickerAndPortfolio(order.getTicker(), order.getPortfolio());

            if(possibleStock.isEmpty())
            {
                //loop through all the executions
                List<Execution> executionList = new ArrayList<Execution>();
                for(ExecutionPayload.Execution ex : payload.getExecutions())
                {
                    System.out.println(ex);
                    Optional<Execution> executionFromDB = executionRepository.findById(ex.getTimestamp());

                    executionList.add(
                            Execution.builder()
                                    .timestamp(ex.getTimestamp())
                                    .price(ex.getPrice())
                                    .quantity(ex.getQuantity())
                                    .trade(trade.get())
                                    .build());
                }

                  //build stock
                Stock stock = Stock.builder()
                        .portfolio(order.getPortfolio())
                        .ticker(order.getTicker())
                        .quantity(payload.getCumulativeQuantity())
                        .value(payload.getCumulativeQuantity() * payload.getCumulativePrice())
                        .build();


                //set executed quantity on main order
                order.setExecutedQuantity(order.getExecutedQuantity()+payload.getCumulativeQuantity());


                //save changes
                executionRepository.saveAll(executionList);
                stockRepository.save(stock);
            }
            //if stock already exists
            else
            {
                Stock savedStock = possibleStock.get();

                List<Execution> executionList = new ArrayList<Execution>();

                //loop through all the executions
                for(ExecutionPayload.Execution ex : payload.getExecutions())
                {
                    Optional<Execution> executionFromDB = executionRepository.findById(ex.getTimestamp());

                    //if execution not already recorded
                    if(executionFromDB.isEmpty())
                    {
                        executionList.add(
                                Execution.builder()
                                        .timestamp(ex.getTimestamp())
                                        .price(ex.getPrice())
                                        .quantity(ex.getQuantity())
                                        .trade(trade.get())
                                        .build());

                        //set quantity and value on stock
                        if(order.getSide() == Side.BUY)
                        {
                            savedStock.setQuantity(savedStock.getQuantity() + ex.getQuantity());
                            savedStock.setValue(savedStock.getValue() + ex.getQuantity() * ex.getPrice());
                        }
                        else
                        {
                            savedStock.setQuantity(savedStock.getQuantity() - ex.getQuantity());
                            savedStock.setValue(savedStock.getValue() - ex.getQuantity() * ex.getPrice());

                            Optional<Portfolio> portfolio=  portfolioRepository.findById(order.getPortfolio().getPortfolioId());

                            if(portfolio.isPresent())
                            {
                                portfolio.get().setAmount(portfolio.get().getAmount() + ex.getQuantity() * ex.getPrice());
                            }
                        }


                        //set executed quantity on main order
                        order.setExecutedQuantity(order.getExecutedQuantity()+ex.getQuantity());

                    }
                }

                //save executions and updated stock
                executionRepository.saveAll(executionList);
                stockRepository.save(savedStock);
            }



            //check quantity
            if(trade.get().getQuantity() >= payload.getCumulativeQuantity())
            {
                trade.get().setStatus(Status.FILLED);
            }


            //loop through original order and check if all filled and update status
            boolean allTradesFilled = true;

            for(Trade t : order.getTrades())
            {
                if(t.getStatus() != Status.FILLED)
                {
                    allTradesFilled = false;
                }
            }


            if(allTradesFilled)
            {
                order.setStatus(Status.FILLED);
                order.setUpdatedAt(Timestamp.from(Instant.now()));

            }
            clientOrderRepository.save(order);
        }
    }

    public List<FetchAllStockResponse> fetchAllClientStock(Long clientId) throws ResourceNotFoundException {
        Optional<Portfolio> portfolio = portfolioRepository.findById(clientId);

        if(portfolio.isEmpty()) throw new ResourceNotFoundException("Client does not exist.");

        List<Stock> allStocks = stockRepository.findAllByPortfolio(portfolio.get());

        List<FetchAllStockResponse> responseList = new ArrayList<>();

        for(Stock stock : allStocks)
        {
            responseList.add(new FetchAllStockResponse(stock.getTicker(), stock.getValue(), stock.getQuantity()));
        }

        return responseList;
    }

    public List<CurrentPricesPayload> getCurrentTickerPrices() {
        MarketDataPayload[] payload = webClientService.getCurrentTickerPricesFromRemote();

        List<CurrentPricesPayload> prices = new ArrayList<>();


        for(MarketDataPayload p : payload)
        {
            prices.add(new CurrentPricesPayload(p.getTICKER(), p.getASK_PRICE(), p.getBID_PRICE(), p.getLAST_TRADED_PRICE()));
        }

        return prices;
    }

    public List<FetchExecutionsResponse> getAllExecutionsOnTrade(String tradeId) throws ResourceNotFoundException {

        Optional<Trade> trade = tradeRepository.findById(tradeId);


        if(trade.isPresent())
        {
            List<Execution> executionsFromDB = executionRepository.findAllByTrade(trade);

            List<FetchExecutionsResponse> responseList = new ArrayList<>();

            for(Execution ex : executionsFromDB)
            {
                responseList.add(new FetchExecutionsResponse(
                        ex.getTimestamp(),
                        ex.getQuantity(),
                        ex.getPrice()
                ));
            }

            return responseList;
        }

        throw new ResourceNotFoundException("Trade does not exist");
    }
}
