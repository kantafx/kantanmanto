package com.kantafx.order.services;

import com.kantafx.order.models.Ticker;
import com.kantafx.order.repositories.AdministratorRepository;
import com.kantafx.order.repositories.TickerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private TickerRepository tickerRepository;

    public Ticker saveTicker(Ticker ticker) {
        return tickerRepository.save(ticker);
    }


    public List<Ticker> fetchAllTickers() {
        return tickerRepository.findAll();
    }
}
