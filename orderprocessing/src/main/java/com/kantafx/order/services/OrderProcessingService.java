package com.kantafx.order.services;

import com.kantafx.order.RabbitConfig;
import com.kantafx.order.exception.ResourceCouldNotBeCreatedException;
import com.kantafx.order.models.*;
import com.kantafx.order.repositories.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderProcessingService {
    @Autowired
    private ClientOrderRepository clientOrderRepository;

    @Autowired
    private TradeRepository tradeRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private PortfolioRepository portfolioRepository;
    @Autowired
    private WebClientService webClientService;

    @Autowired
    private RabbitTemplate template;

    private boolean shouldProceedToCreateOrder(ClientOrder order, OrderBookItem book, double price) {
        // params: orderbook, price, order
        // for limit order
        if (order.getOrderType() == OrderType.LIMIT) {
            // on sell, the price on the orderbook should not be less than the order price
            // on buy, the price on the orderbook should not be greater than the order price
            if (order.getSide() == Side.SELL) return book.getData().getPrice() >= price;
            else return book.getData().getPrice() <= price;
        }
        // anything else, proceed with the order
        return true;
    }

    private void createTrade(ClientOrder order, double price, Integer quantity, Exchange exchange) {
        //create request to be sent to the exchange
        ExchangeOrderRequest orderRequest = new ExchangeOrderRequest(
                order.getTicker().getTickerLable(),
                quantity,
                price,
                order.getSide(),
                order.getOrderType());


        // send order to exchange and receive response
        String response = webClientService.sendOrderToExchange(exchange.getEndpoint(), orderRequest);
        String tradeIdResponseFromExchange = response.substring(1, response.length() - 1);

        // build up trade to be stored in our system
        Trade trade = Trade.builder()
                .exchangeId(tradeIdResponseFromExchange)
                .exchangeName(exchange.getId())
                .quantity(quantity)
                .price(price)
                .order(order)
                .status(Status.CREATED)
                .build();


        // save trade
        tradeRepository.save(trade);

        //publish trade to queue
        Map<String, Object> payload = Map.of("command", "add", "body", Map.of("order_id", tradeIdResponseFromExchange, "exchange_id", exchange.getId()));
        template.convertAndSend(RabbitConfig.ORDER_CREATED_EXCHANGE, RabbitConfig.ORDER_CREATED_ROUTING_KEY, payload);


        // update trades
        order.getTrades().add(trade);
        clientOrderRepository.save(order);

        // update portfolio balance
        updatePortfolioBalance(order, trade);
    }

    public String processOrder(ClientOrder order, List<MarketData> marketDataList) throws ResourceCouldNotBeCreatedException {
        //save order
        ClientOrder savedOrder = clientOrderRepository.save(order);

        if (Objects.nonNull(savedOrder)) {
            //total quantity to be executed from order
            int totalQuantity = order.getQuantity();

            if (marketDataList.size() == 1) {
                //if only one exchange exists in marketData list, create single order
                MarketData md = marketDataList.get(0);
                createTrade(order, calculatePrice(order, md), totalQuantity, md.getExchange());
            } else {
                if (order.getOrderType() == OrderType.LIMIT) {
                    boolean isMulti = processMultiTrade(order, marketDataList);
                    return isMulti ? "Multi-leg order created successfully" : "Single-leg order created successfully";
                } else {
                    MarketData md = recommendMarketData(marketDataList, order.getSide());
                    createTrade(order, calculatePrice(order, md), totalQuantity, md.getExchange());
                }
            }
            return "Single-leg order created successfully";
        }

        throw new ResourceCouldNotBeCreatedException("Failed to create order");
    }

    private MarketData recommendMarketData(List<MarketData> mdList, Side clientSide) {
        if (mdList.size() == 1) return mdList.get(0);

        MarketData md0 = mdList.get(0);
        MarketData md1 = mdList.get(1);

        // cheapest for buy and expensive for sell
        // compare bidprice for sell, askprice for buy
        if (clientSide == Side.SELL) {
            if (md1.getBidPrice() >= md0.getBidPrice()) return md1;
            else return md0;
        } else {
            if (md1.getAskPrice() <= md0.getAskPrice()) return md1;
            else return md0;
        }
    }

    private MarketData getMarketDataForExchange(List<MarketData> mdList, Exchange ex) {
        for (var md : mdList) {
            if (md.getExchange().getId() == ex.getId()) return md;
        }
        return mdList.get(0);
    }

    private boolean processMultiTrade(ClientOrder order, List<MarketData> mdList) {
        boolean hasHitFirst = false;
        boolean hasHitSecond = false;

        // 1. get user ticker
        String ticker = String.valueOf(order.getTicker().getTickerLable()).toUpperCase();

        // 2. get exchanges data based on side
        // ie. get sell when user wants to buy, get buy when user wants to sell
        String side = order.getSide() == Side.BUY ? "SELL" : "BUY";

        // 3. get orderbook based on ticker and side
        List<List<OrderBookItem>> orderBooks = new ArrayList<>();

        if(!OrderBookCacheRepository.cachedOrderBook.containsKey(ticker)) {
            MarketData md = recommendMarketData(mdList, order.getSide());
            createTrade(order, calculatePrice(order, md), order.getQuantity(), md.getExchange());
            return false;
        }

        for (var entry : OrderBookCacheRepository.cachedOrderBook.get(ticker).entrySet()) {
            orderBooks.add(entry.getValue().get(side));
        }

        // 4. continuously perform order from the lowest index upwards until everything is fulfilled
        int targetQuantity = order.getQuantity();
        if (orderBooks.size() == 1) {
            // single trade
            int i = 0;
            List<OrderBookItem> orderBook = orderBooks.get(0);
            while (i < orderBook.size() && targetQuantity > 0) {
                OrderBookItem data = orderBook.get(i);
                int qty = data.getData().getQuantity();

                if (qty <= targetQuantity) {
                    MarketData md = getMarketDataForExchange(mdList, data.getExchange());
                    double price = calculatePrice(order, md);
                    if (shouldProceedToCreateOrder(order, data, price)) {
                        createTrade(order, price, qty, data.getExchange());
                        targetQuantity -= qty;
                    }
                }
                i += 1;
            }
        } else {
            // multi trade
            // nb: orderBooks can only be of maxSize 2 for now since we are using a max of two exchanges
            int i = 0;
            List<OrderBookItem> orderBookI = orderBooks.get(0);

            int j = 0;
            List<OrderBookItem> orderBookJ = orderBooks.get(1);

            while (targetQuantity > 0 && i < orderBookI.size() && j < orderBookJ.size()) {
                double iPrice = orderBookI.get(i).getData().getPrice();
                double jPrice = orderBookJ.get(j).getData().getPrice();

                while (i < orderBookI.size() && iPrice < jPrice && targetQuantity > 0) {
                    OrderBookItem data = orderBookI.get(i);
                    int qty = data.getData().getQuantity();

                    if (qty <= targetQuantity) {
                        MarketData md = getMarketDataForExchange(mdList, data.getExchange());

                        double price = calculatePrice(order, md);
                        if (shouldProceedToCreateOrder(order, data, price)) {
                            createTrade(order, price, qty, data.getExchange());
                            targetQuantity -= qty;
                            hasHitFirst = true;
                        }

                    }

                    iPrice = data.getData().getPrice();
                    i += 1;
                }

                while (j < orderBookJ.size() && jPrice < iPrice && targetQuantity > 0) {
                    OrderBookItem data = orderBookJ.get(j);
                    int qty = data.getData().getQuantity();

                    if (qty <= targetQuantity) {

                        MarketData md = getMarketDataForExchange(mdList, data.getExchange());

                        double price = calculatePrice(order, md);
                        if (shouldProceedToCreateOrder(order, data, price)) {
                            createTrade(order, price, qty, data.getExchange());
                            targetQuantity -= qty;
                            hasHitSecond = true;
                        }
                    }

                    jPrice = data.getData().getPrice();
                    j += 1;
                }

                if (i >= orderBookI.size() - 1 || j >= orderBookJ.size() - 1) {
                    List<OrderBookItem> orderBook;
                    int k;
                    if (i >= orderBookI.size() - 1) {
                        orderBook = orderBooks.get(1);
                        k = j;
                    } else {
                        orderBook = orderBooks.get(0);
                        k = i;
                    }

                    while (k < orderBook.size() && targetQuantity > 0) {
                        OrderBookItem data = orderBook.get(k);
                        int qty = data.getData().getQuantity();

                        if (qty <= targetQuantity) {

                            MarketData md = getMarketDataForExchange(mdList, data.getExchange());
                            double price = calculatePrice(order, md);
                            if (shouldProceedToCreateOrder(order, data, price)) {
                                createTrade(order, price, qty, data.getExchange());
                                targetQuantity -= qty;
                            }
                        }

                        k += 1;
                    }
                }


            }
        }

        if (targetQuantity > 0) {
            Random rand = new Random();
            MarketData md = mdList.get(rand.nextInt(mdList.size()));

            createTrade(order, calculatePrice(order, md), targetQuantity, md.getExchange());
            targetQuantity = 0;
        }

//        System.out.println("orderbook size was one, bought: " + (order.getQuantity() - targetQuantity));
        return hasHitFirst && hasHitSecond;
    }


    private void updatePortfolioBalance(ClientOrder order, Trade trade) {
        if (order.getSide() == Side.BUY) {
            Portfolio userPortfolio = order.getPortfolio();
            userPortfolio.setAmount(userPortfolio.getAmount() - trade.getQuantity() * trade.getPrice());
            portfolioRepository.save(userPortfolio);
        }
    }


    private double calculatePrice(ClientOrder order, MarketData md) {
        double price;
        if (order.getOrderType() == OrderType.MARKET) {
            price = 0;
        } else {
            price = order.getPrice();
        }

        return price;
    }
}

