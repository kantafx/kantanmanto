package com.kantafx.order.services;

import com.kantafx.order.RabbitConfig;
import com.kantafx.order.payloads.ExecutionPayload;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class OrderExecutionListener {
    @Autowired
    private ClientService clientService;

    @RabbitListener(queues = RabbitConfig.ORDER_EXECUTED_QUEUE)
    public void listener(Message message) {
        Map<String, Object> payload = (Map<String, Object>) new Jackson2JsonMessageConverter().fromMessage(message, ExecutionPayload.class);
        ExecutionPayload execution = ExecutionPayload.fromHashMap(payload);
        System.out.println("execution received");
        System.out.println(execution);

        clientService.trackOrder(execution);

        System.out.println("execution tracked");
    }
}
