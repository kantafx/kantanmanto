package com.kantafx.order.services;

import com.kantafx.order.models.Exchange;
import com.kantafx.order.models.ExchangeOrderRequest;
import com.kantafx.order.models.MarketData;
import com.kantafx.order.payloads.CurrentPricesPayload;
import com.kantafx.order.payloads.MarketDataPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class WebClientService {
    @Value("${token}")
    public String TOKEN;

    @Value("${market-data-endpoint}")
    public String MARKET_DATA_ENDPOINT;

    @Autowired
    private WebClient webClient;


    public String queryMarketDataForExchangeEndpoint(String exchangeName)
    {
        String endpoint = webClient.get().uri(MARKET_DATA_ENDPOINT+"/" + "exchanges/"+ exchangeName + "/endpoint")
                .retrieve().bodyToMono(String.class).block();

        return endpoint;
    }


    public Boolean sendCancellationRequestToExchange(String endpoint, String exchangeOrderId)
    {
        String response = webClient.delete().uri(endpoint+"/" + TOKEN + "/order/" + exchangeOrderId)
                .retrieve().bodyToMono(String.class).block();

        return Boolean.valueOf(response);
    }

    public MarketDataPayload[] getAvailableMarketPricesFromRemote() {
        WebClient.ResponseSpec response = webClient.get().uri(MARKET_DATA_ENDPOINT+"/availableData").retrieve();
        MarketDataPayload[] data = response.bodyToMono(MarketDataPayload[].class).block();
        return data;
    }

    public String sendOrderToExchange(String endpoint, ExchangeOrderRequest orderRequest)
    {
        //send post request and receive string response
        String responseFromExchange = webClient.post().uri(endpoint+"/" + TOKEN + "/order").body(Mono.just(orderRequest), ExchangeOrderRequest.class)
                .retrieve().bodyToMono(String.class).block();

        //Remove quotes from response string
        return responseFromExchange;
    }

    public MarketDataPayload[] getCurrentTickerPricesFromRemote() {
        WebClient.ResponseSpec response = webClient.get().uri("https://exchange.matraining.com/pd").retrieve();
        MarketDataPayload[] data = response.bodyToMono(MarketDataPayload[].class).block();
        return data;
    }
}
