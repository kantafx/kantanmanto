package com.kantafx.order.services;

import com.kantafx.order.RabbitConfig;
import com.kantafx.order.payloads.TimestampedMarketData;
import com.kantafx.order.repositories.MarketDataCacheRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MarketDataListener {

    @Autowired
    private MarketDataCacheRepository marketDataCacheRepository;

    @RabbitListener(queues = RabbitConfig.MARKET_DATA_QUEUE)
    public void listener(TimestampedMarketData message) {
        MarketDataCacheRepository.setCurrentMarketData(message);
    }
}
