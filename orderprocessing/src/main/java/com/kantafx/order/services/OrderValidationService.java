package com.kantafx.order.services;

import com.kantafx.order.models.*;
import com.kantafx.order.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderValidationService {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private MarketDataService marketDataService;

    public List<MarketData> getValidExchangesForOrder(ClientOrder order) {
        List<MarketData> marketData = marketDataService.getMarketData(order.getTicker());
        return marketData.stream().filter(md -> validateOrderWithMarketData(order, md)).toList();
    }

    public Boolean adequateBuyingFundsValidation(ClientOrder order) {
        if (order.getSide().equals(Side.BUY) && order.getOrderType().equals(OrderType.LIMIT)) {
            double amount = order.getPortfolio().getAmount();
            double orderValue = order.getPrice() * order.getQuantity();
            return orderValue <= amount;
        }
        return true;
    }

    public Boolean stockOwnershipValidation(ClientOrder order) {
        if (order.getSide().equals(Side.SELL)) {
            Optional<Stock> stock = stockRepository.findByTickerAndPortfolio
                    (order.getTicker(), order.getPortfolio());

            if (stock.isEmpty()) {
                return false;
            }

            return stock.get().getQuantity() >= order.getQuantity();
        }
        return true;
    }

    public Boolean validateOrderWithMarketData(ClientOrder order, MarketData marketData) {
        if (order.getOrderType().equals(OrderType.LIMIT)) {
            return validateLimitOrder(order, marketData);
        } else {
            return validateMarketOrder(order, marketData);
        }
    }

    private Boolean validateLimitOrder(ClientOrder order, MarketData marketData) {
        double priceDifference;
        boolean isWithinLimit;
        if (order.getSide() == Side.BUY) {
            priceDifference = order.getPrice() - marketData.getAskPrice();
            isWithinLimit = withinBuyLimit(order, marketData);
        } else {
            priceDifference = order.getPrice() - marketData.getBidPrice();
            isWithinLimit = withinSellLimit(order, marketData);
        }
        return Math.abs(priceDifference) <= marketData.getMaxPriceShift() && adequateBuyingFundsValidation(order) && stockOwnershipValidation(order) && isWithinLimit;
    }

    private Boolean validateMarketOrder(ClientOrder order, MarketData marketData) {
        order.setPrice(0.00);
        double volume;
        boolean isWithinLimit;
        if (order.getSide() == Side.BUY) {
            if (marketData.getAskPrice() == 0) return false;
            volume = order.getQuantity() * marketData.getAskPrice();
            isWithinLimit = withinBuyLimit(order, marketData);
        } else {
            if (marketData.getBidPrice() == 0) return false;
            volume = order.getQuantity() * marketData.getBidPrice();
            isWithinLimit = withinSellLimit(order, marketData);
        }
        return volume <= order.getPortfolio().getAmount() && stockOwnershipValidation(order) && isWithinLimit;
    }

    private Boolean withinSellLimit(ClientOrder order, MarketData marketData) {
        return order.getQuantity() <= marketData.getSellLimit();
    }

    private Boolean withinBuyLimit(ClientOrder order, MarketData marketData) {
        return order.getQuantity() <= marketData.getBuyLimit();
    }

}
