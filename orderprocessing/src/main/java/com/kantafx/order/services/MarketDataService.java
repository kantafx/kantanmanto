package com.kantafx.order.services;

import com.kantafx.order.models.MarketData;
import com.kantafx.order.models.Ticker;
import com.kantafx.order.payloads.TimestampedMarketData;
import com.kantafx.order.repositories.MarketDataCacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class MarketDataService {

    @Autowired
    private WebClientService webClientService;

    @Autowired
    WebClient httpClient;

    private MarketData convertMarketPayloadToData(TimestampedMarketData.MarketDataPayload payload) {
        return MarketData.builder().
                ticker(payload.getTICKER())
                .sellLimit(payload.getSELL_LIMIT())
                .lastTradedPrice(payload.getLAST_TRADED_PRICE())
                .maxPriceShift(payload.getMAX_PRICE_SHIFT())
                .askPrice(payload.getASK_PRICE())
                .bidPrice(payload.getBID_PRICE())
                .buyLimit(payload.getBUY_LIMIT())
                .exchange(payload.getExchange())
                .build();
    }

    public List<MarketData> getMarketData(Ticker ticker) {
        List<TimestampedMarketData.MarketDataPayload> marketDataPayload = MarketDataCacheRepository.currentMarketData.getMarketData().stream().filter(mData -> mData.getTICKER().equals(ticker.getTickerLable().toString())).toList();
        if (marketDataPayload.isEmpty() || marketDataCacheIsOutOfDate()) {
            return getAvailableMarketPricesFromRemote();
        } else {
            return marketDataPayload.stream().map(this::convertMarketPayloadToData).toList();
        }
    }

    private List<MarketData> getAvailableMarketPricesFromRemote() {
        String marketDataEndpoint = "http://localhost:8082/availableData";
        WebClient.ResponseSpec response = httpClient.get().uri(marketDataEndpoint).retrieve();
        TimestampedMarketData.MarketDataPayload[] data = response.bodyToMono(TimestampedMarketData.MarketDataPayload[].class).block();
        if (data == null) return new ArrayList<>();
        return Arrays.stream(data).map(this::convertMarketPayloadToData).toList();
    }

    private Boolean marketDataCacheIsOutOfDate() {
        return (new Date().getTime() - MarketDataCacheRepository.currentMarketData.getTimestamp().getTime()) / 1000 > 2; // more than 2 seconds
    }
}
