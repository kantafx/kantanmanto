package com.kantafx.order.payloads;

import com.kantafx.order.models.ClientOrder;

public record TradeRecord(
        String exchangeName,
        ClientOrder clientOrder,
        Integer quantity,
        Double price
) {
}
