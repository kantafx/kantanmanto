package com.kantafx.order.payloads;

import com.kantafx.order.models.Ticker;

public record FetchAllStockResponse(
        Ticker ticker,
        Double value,
        Integer quantity
) {
}
