package com.kantafx.order.payloads;

public record FetchPortfoliosByClientRequest(
        String email
) {
}
