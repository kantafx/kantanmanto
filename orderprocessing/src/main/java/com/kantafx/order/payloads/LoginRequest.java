package com.kantafx.order.payloads;

public record LoginRequest(
        String email,
        String password
) {
}
