package com.kantafx.order.payloads;

public record CurrentPricesPayload(
        String tickerLable,
        Double askPrice,
        Double bidPrice,
        Double lastTradedPrice
) {
}
