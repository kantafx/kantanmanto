package com.kantafx.order.payloads;

import com.kantafx.order.models.Side;
import com.kantafx.order.models.TickerLable;

public record ClientPendingOrderResponse(
        TickerLable tickerLable,
        String description,
        Side side,
        Integer quantity,
        Double price
) {
}
