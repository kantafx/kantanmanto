package com.kantafx.order.payloads;

public record ClientRegistrationRequest(
       String email,

       String name,

       String password
) {
}
