package com.kantafx.order.payloads;

public record NewPortfolioRequest(
        String email,
        String portfolioName,
        Double amount
) {
}
