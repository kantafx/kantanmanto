package com.kantafx.order.payloads;

import com.kantafx.order.models.TickerLable;

public record StockRequest(
        TickerLable tickerLable,

        Long portfolioId,

        Integer quantity,

        Double price
        ) {
}
