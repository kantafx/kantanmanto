package com.kantafx.order.payloads;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TimestampedOrderBook {
    private List<OrderBookPayload> data;
    private Timestamp timestamp;
}
