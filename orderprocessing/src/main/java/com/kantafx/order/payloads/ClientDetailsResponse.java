package com.kantafx.order.payloads;


public record ClientDetailsResponse(
        String name,
        Long clientId,
        String email
) {
}