package com.kantafx.order.payloads;

import org.springframework.http.HttpStatus;

public record ResponseBuilder(
        HttpStatus status,
        String message
) {
}
