package com.kantafx.order.payloads;

import java.sql.Timestamp;

public record FetchExecutionsResponse(
        Timestamp timestamp,
        Integer quantity,
        Double price
) {
}
