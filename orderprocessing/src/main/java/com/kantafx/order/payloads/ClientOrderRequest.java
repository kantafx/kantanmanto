package com.kantafx.order.payloads;

import com.kantafx.order.models.OrderType;
import com.kantafx.order.models.Side;
import com.kantafx.order.models.TickerLable;


public record ClientOrderRequest(
        TickerLable tickerLable,
        Long portfolioId,
        Integer quantity,
        Double price,
        Side side,
        OrderType orderType
) {
}
