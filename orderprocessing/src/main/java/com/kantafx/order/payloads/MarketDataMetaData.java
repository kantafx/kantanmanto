package com.kantafx.order.payloads;

import java.sql.Timestamp;
import java.util.List;

public record MarketDataMetaData(
        List<MarketDataPayload> data,
        Timestamp timestamp) {

}
