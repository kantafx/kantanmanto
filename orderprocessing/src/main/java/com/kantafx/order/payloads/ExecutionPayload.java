package com.kantafx.order.payloads;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExecutionPayload {
    private String product;
    private int quantity;
    private double price;
    private String side;
    private List<Execution> executions;
    private String orderID;
    private String orderType;
    private int cumulativeQuantity;
    private double cumulativePrice;


    @Builder
    @Data
    public static class Execution {
        private Timestamp timestamp;
        private double price;
        private int quantity;

        public static Execution fromHashMap(Map<String, Object> json) {
            DateFormat mdyFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Timestamp timestamp;
            try {
                final Date d = mdyFormat.parse((String) json.get("timestamp"));
                timestamp = new Timestamp(d.getTime());
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            return Execution.builder().timestamp(timestamp).price((Double.valueOf((Integer) json.get("price")))).quantity((Integer) json.get("quantity")).build();
        }
    }

    public static ExecutionPayload fromHashMap(Map<String, Object> json) {
        return ExecutionPayload.builder()
                .product((String) json.get("product"))
                .quantity((Integer) json.get("quantity"))
                .price((Double) json.get("price"))
                .side((String) json.get("side"))
                .executions(((ArrayList) json.get("executions")).stream().map(e -> Execution.fromHashMap((Map<String, Object>) e)).toList())
                .orderID((String) json.get("orderID"))
                .orderType((String) json.get("orderType"))
                .cumulativeQuantity((Integer) json.get("cumulatitiveQuantity"))
                .cumulativePrice((Double) json.get("cumulatitivePrice"))
                .build();
    }
}
