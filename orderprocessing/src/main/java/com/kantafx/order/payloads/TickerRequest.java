package com.kantafx.order.payloads;

import com.kantafx.order.models.TickerLable;

public record TickerRequest(
        TickerLable tickerLable,

        String description
) {
}
