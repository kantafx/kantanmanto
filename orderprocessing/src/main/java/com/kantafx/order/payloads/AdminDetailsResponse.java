package com.kantafx.order.payloads;

public record AdminDetailsResponse(
        String email
) {
}
