package com.kantafx.order.payloads;

import com.kantafx.order.models.*;

import java.sql.Timestamp;

public interface FetchAllClientOrdersResponse {
    String getExchangeId();
    Long getPortfolioId();
    String getName();
    TickerLable getTicker();
    Integer getQuantity();
    Double getPrice();
    Side getSide();
    Timestamp getCreatedAt();
    Timestamp getUpdatedAt();
    Integer getExecutedQuantity();
    Status getStatus();
    OrderType getOrderType();
}
