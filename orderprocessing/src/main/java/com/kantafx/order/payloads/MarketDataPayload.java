package com.kantafx.order.payloads;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kantafx.order.models.Exchange;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MarketDataPayload {
    @JsonProperty
    private String TICKER;

    @JsonProperty
    private Integer SELL_LIMIT;

    @JsonProperty
    private Double LAST_TRADED_PRICE;

    @JsonProperty
    private Double MAX_PRICE_SHIFT;

    @JsonProperty
    private Double ASK_PRICE;

    @JsonProperty
    private Double BID_PRICE;

    @JsonProperty
    private Integer BUY_LIMIT;
    private Exchange exchange;

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }
}