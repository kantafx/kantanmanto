package com.kantafx.order.payloads;

import com.kantafx.order.models.OrderType;
import com.kantafx.order.models.Side;
import com.kantafx.order.models.TickerLable;

import java.sql.Timestamp;

public interface FetchAllClientOrdersByPortfolioWherePendingResponse {
    String getExchangeId();
    String getExchangeName();
    TickerLable getTicker();
    Integer getQuantity();
    Timestamp getCreatedAt();
    Double getPrice();
    Side getSide();
    OrderType getOrderType();
}
