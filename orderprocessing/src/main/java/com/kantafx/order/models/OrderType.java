package com.kantafx.order.models;

public enum OrderType {
    MARKET,
    LIMIT
}
