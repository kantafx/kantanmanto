package com.kantafx.order.models;

public enum Status {
    FULFILLED,
    CREATED,
    FILLED,
    FAILED,
    CANCELLED
}
