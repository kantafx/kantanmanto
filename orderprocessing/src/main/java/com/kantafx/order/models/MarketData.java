package com.kantafx.order.models;

import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class MarketData {
    private Long id;

    private String ticker;

    private Integer sellLimit;

    private Double lastTradedPrice;

    private Double maxPriceShift;

    private Double askPrice;

    private Double bidPrice;

    private Integer buyLimit;

    private Timestamp createdAt;

    private Exchange exchange;
}
