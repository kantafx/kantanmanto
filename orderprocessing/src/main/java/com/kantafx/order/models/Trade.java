package com.kantafx.order.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Trade {

    @Id
    @Column(nullable = false)
    private String exchangeId;

    @ManyToOne
    @JoinColumn(referencedColumnName = "orderId", nullable = false)
    private ClientOrder order;

    @Column(name = "exchangeName")
    private String exchangeName;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price")
    private Double price;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
