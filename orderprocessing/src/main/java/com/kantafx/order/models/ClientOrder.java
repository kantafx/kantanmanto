package com.kantafx.order.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientOrder {
    @Id
    @SequenceGenerator(
            name = "order_id_sequence",
            sequenceName = "order_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "order_id_sequence"
    )
    private Long orderId;


    @ManyToOne
    @JoinColumn(name="tickerLabel")
    private Ticker ticker;

    @ManyToOne
    @JoinColumn(name="portfolioId")
    private Portfolio portfolio;

    @Column(nullable = false)
    private Integer quantity;

    @OneToMany
    private List<Trade> trades;

    @Column(nullable = false)
    private Double price;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Side side;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Status status;

    @Column(nullable = false)
    private Timestamp createdAt;

    @Column
    private Timestamp updatedAt;

    @Column(nullable = false)
    private Integer executedQuantity;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OrderType orderType;
}
