package com.kantafx.order.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Execution {
    @Id
    private Timestamp timestamp;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="price")
    private Double price;

    @ManyToOne
    @JoinColumn(referencedColumnName = "exchangeId", nullable = false)
    private Trade trade;
}
