package com.kantafx.order.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client {
    @Id
    @SequenceGenerator(
            name = "client_id_sequence",
            sequenceName = "client_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "client_id_sequence"
    )


    private Long clientId;

    @NotBlank(message = "Please Add you email")
    @Email
    @Column(nullable = false, unique = true)
    private String email;

    @NotBlank(message = "Please Add you name")
    @Column(nullable = false)
    private String name;

    @NotBlank(message = "Please Add you password")
    @Column(nullable = false)
    private String password;

//    @OneToMany(mappedBy = "client")
//    private List<Portfolio> portfolio;


}
