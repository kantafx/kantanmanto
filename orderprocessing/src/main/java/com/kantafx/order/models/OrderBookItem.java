package com.kantafx.order.models;

import com.kantafx.order.payloads.OrderBookPayload;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Objects;

@Builder
@Getter
@AllArgsConstructor
public class OrderBookItem implements Comparable<OrderBookItem> {
    private OrderBookPayload.ProductInformation data;
    private Exchange exchange;

    @Override
    public String toString() {
        return String.format("OrderBookItem(ticker=%s, price=%s, quantity=%s, side=%s)", data.getProduct(), data.getPrice(), data.getQuantity(), data.getSide());
    }

    @Override
    public int hashCode() {
        return Objects.hash(data.getOrderID());
    }

    @Override
    public boolean equals(Object obj) {
        OrderBookItem otherItem = (OrderBookItem) obj;

        return (data.getOrderID() == otherItem.data.getOrderID());
    }

    @Override
    public int compareTo(OrderBookItem otherItem) {
        // sort in desc for sell
        if(data.getSide() == "SELL") {
            if(otherItem.data.getPrice() > data.getPrice()) return -1;
            else if(otherItem.data.getPrice() < data.getPrice()) return 1;
            else return 0;
        }
        else{
            // sort in asc for buy
            if (otherItem.data.getPrice() < data.getPrice()) return -1;
            else if (otherItem.data.getPrice() > data.getPrice()) return 1;
            else return 0;
        }
    }
}
