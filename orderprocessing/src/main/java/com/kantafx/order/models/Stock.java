package com.kantafx.order.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Stock {
    @Id
    @SequenceGenerator(
            name = "stock_id_sequence",
            sequenceName = "stock_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "stock_id_sequence"
    )
    private Long stockId;



    @ManyToOne
    @JoinColumn(name="portfolioId")
    private Portfolio portfolio;

    @ManyToOne
    @JoinColumn(name="tickerLabel")
    private Ticker ticker;



    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double value;
}
