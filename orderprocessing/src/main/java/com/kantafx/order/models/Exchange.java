package com.kantafx.order.models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Exchange {
    private String id;

    private String endpoint;

    private Boolean active;

    private Timestamp createdAt;

    private Timestamp updatedAt;

}
