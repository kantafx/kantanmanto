package com.kantafx.order.models;

public record ExchangeOrderRequest(
        TickerLable product,
        Integer quantity,
        Double price,
        Side side,
        OrderType type
        ) {
}
