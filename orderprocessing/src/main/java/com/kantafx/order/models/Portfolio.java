package com.kantafx.order.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Portfolio {
    @Id
    @SequenceGenerator(
            name = "portfolio_id_sequence",
            sequenceName = "portfolio_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "portfolio_id_sequence"
    )
    private Long portfolioId;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="clientId")
    private Client client;

    @Column(nullable = false)
    private Boolean isDefault;

    @NotBlank(message = "Please Add you name")
    @Column(nullable = false)
    private String portfolioName;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false, updatable = false)
    private Timestamp createdAt;

    @OneToMany(mappedBy = "portfolio")
    private List<Stock> stock;

//    @OneToMany(mappedBy = "portfolio")
//    private List<ClientOrder> clientOrder;
}
