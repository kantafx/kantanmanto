package com.kantafx.order.models;


import java.util.Comparator;

public class OrderBookItemComparator implements Comparator<OrderBookItem> {
    @Override
    public int compare(OrderBookItem o1, OrderBookItem o2) {
        return o1.compareTo(o2);
    }
}
