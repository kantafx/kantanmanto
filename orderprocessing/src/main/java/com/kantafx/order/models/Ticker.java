package com.kantafx.order.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Ticker {
    @Id
    @Column(nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    private TickerLable tickerLable;

    @NotBlank(message = "Description must not be blank")
    @Column(nullable = false)
    private String description;

//    @OneToMany(mappedBy = "ticker")
//    private List<Stock> stock;
//
//
//    @OneToMany(mappedBy = "ticker")
//    private List<ClientOrder> clientOrder;
}
