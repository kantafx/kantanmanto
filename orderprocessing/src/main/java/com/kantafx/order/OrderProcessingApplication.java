package com.kantafx.order;

import com.kantafx.order.models.Administrator;
import com.kantafx.order.models.Exchange;
import com.kantafx.order.models.Ticker;
import com.kantafx.order.models.TickerLable;
import com.kantafx.order.payloads.OrderBookPayload;
import com.kantafx.order.repositories.AdministratorRepository;
import com.kantafx.order.repositories.OrderBookCacheRepository;
import com.kantafx.order.repositories.TickerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@SpringBootApplication
public class OrderProcessingApplication {
    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private TickerRepository tickerRepository;

    public static void main(String[] args) {
        SpringApplication.run(OrderProcessingApplication.class);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("GET", "POST","PUT", "DELETE").allowedOrigins("http://localhost:4200");
            }
        };
    }


    @Bean
    WebClient httpClient() {
        return WebClient.create();
    }

    @Bean
    CommandLineRunner runner() {
        return args -> {
            Administrator administrator = new Administrator("tlc3@turntabl.io", "kantamanto");
            administratorRepository.save(administrator);

            Ticker aapl = new Ticker(TickerLable.AAPL, "Apple Inc.");
            Ticker ibm = new Ticker(TickerLable.IBM, "International Business Machines Corporation");
            Ticker msft = new Ticker(TickerLable.MSFT, "Microsoft Corporation");
            Ticker amzn = new Ticker(TickerLable.AMZN, "Amazon.com, Inc.");
            Ticker tsla = new Ticker(TickerLable.TSLA, "Tesla, Inc.");
            Ticker nflx = new Ticker(TickerLable.NFLX, "Netflix, Inc.");
            Ticker orcl = new Ticker(TickerLable.ORCL, "Oracle Corporation");
            Ticker googl = new Ticker(TickerLable.GOOGL, "Google LLC");

            tickerRepository.save(aapl);
            tickerRepository.save(ibm);
            tickerRepository.save(msft);
            tickerRepository.save(amzn);
            tickerRepository.save(tsla);
            tickerRepository.save(nflx);
            tickerRepository.save(orcl);
            tickerRepository.save(googl);


//            Exchange exchange1 = new Exchange("MAL1", "https://exchange.matraining.com", true, Timestamp.from(Instant.now()), null);
//
//            Exchange exchange2 = new Exchange("MAL2", "https://exchange2.matraining.com", true, Timestamp.from(Instant.now()), null);
//
//
//            exchangeRepository.save(exchange1);
//            exchangeRepository.save(exchange2);
        };
    }
}
