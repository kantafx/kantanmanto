package com.kantafx.order.exception;

public class ResourceCouldNotBeCreatedException extends Exception{
    public ResourceCouldNotBeCreatedException(String message)
    {
        super(message);
    }
}
