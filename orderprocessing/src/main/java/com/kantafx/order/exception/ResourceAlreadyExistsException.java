package com.kantafx.order.exception;

public class ResourceAlreadyExistsException extends Exception{
    public ResourceAlreadyExistsException(String message){
        super(message);
    }
}
