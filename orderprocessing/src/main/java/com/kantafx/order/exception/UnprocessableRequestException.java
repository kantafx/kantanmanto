package com.kantafx.order.exception;

public class UnprocessableRequestException extends Exception{
    public UnprocessableRequestException(String message)
    {
        super(message);
    }
}
