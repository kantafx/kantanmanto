package com.kantafx.order.repositories;

import com.kantafx.order.models.OrderBookItem;
import com.kantafx.order.models.OrderBookItemComparator;
import com.kantafx.order.payloads.OrderBookPayload;
import com.kantafx.order.services.ExchangeWorker;
import com.kantafx.order.payloads.TimestampedOrderBook;

import java.sql.Timestamp;
import java.util.*;

public class OrderBookCacheRepository {

    public static Map<String, Map<String, Map<String, ArrayList<OrderBookItem>>>> cachedOrderBook = new HashMap<>();

    public static Timestamp timestamp;

    public static void populateOrderBooks(TimestampedOrderBook payload) {
        cachedOrderBook = new HashMap<>();

        for (OrderBookPayload orderBook : payload.getData()) {
            List<OrderBookPayload.ProductInformation> fullOrderBook = orderBook.getFullOrderBook();

            String exchange = ExchangeWorker.buildKey(orderBook.getExchange().getId(), orderBook.getExchange().getEndpoint());
            String ticker = "";

            for (OrderBookPayload.ProductInformation orderBookItem : fullOrderBook) {
                ticker = orderBookItem.getProduct().toUpperCase();

                if (!cachedOrderBook.containsKey(ticker)) {
                    cachedOrderBook.put(orderBookItem.getProduct(), new HashMap<>());
                } else {
                    String side = orderBookItem.getSide().toUpperCase();
                    if (!cachedOrderBook.get(ticker).containsKey(exchange)) {
                        cachedOrderBook.get(ticker).put(exchange, new HashMap<>());
                    } else {
                        if (!cachedOrderBook.get(ticker).get(exchange).containsKey(side)) {
                            cachedOrderBook.get(ticker).get(exchange).put(side, new ArrayList<>());
                        }

                        cachedOrderBook.get(ticker).get(exchange).get(side).add(new OrderBookItem(orderBookItem, orderBook.getExchange()));
                    }
                }
            }

            if(cachedOrderBook.containsKey(ticker) && cachedOrderBook.get(ticker).containsKey(exchange)){
                for (var entry : cachedOrderBook.get(ticker).get(exchange).entrySet()) {
                    entry.getValue().sort(new OrderBookItemComparator());
                }
            }

        }
        timestamp = payload.getTimestamp();
    }
}
