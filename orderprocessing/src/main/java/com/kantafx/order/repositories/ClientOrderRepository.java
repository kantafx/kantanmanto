package com.kantafx.order.repositories;

import com.kantafx.order.models.*;
import com.kantafx.order.payloads.FetchAllClientOrdersByPortfolioWherePendingResponse;
import com.kantafx.order.payloads.FetchAllClientOrdersResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ClientOrderRepository extends JpaRepository<ClientOrder, Long> {
     ClientOrder findByOrderType(OrderType orderType);


    @Query(nativeQuery = true,
            value = "select o.exchange_id as ExchangeId,  o.portfolio_id as PortfolioId " +
                    " ,c.\"name\" as Name, o.ticker_label as Ticker, o.quantity as Quantity, o.price as Price, o.side as Side, o.created_at as CreatedAt " +
                    ",o.updated_at as UpdatedAt,o.executed_quantity as ExecutedQuantity, o.status as Status , o.order_type as OrderType " +
                    "from client_order o join portfolio p on " +
                    "o.portfolio_id = p.portfolio_id join client c on p.client_id = c.client_id where c.client_id = ?1")
    List<FetchAllClientOrdersResponse> findAllByClientId(Long clientId);

    List<ClientOrder> findAllByPortfolio(Portfolio portfolio);

    void deleteByPortfolio(Optional<Portfolio> portfolio);

    void deleteAllByPortfolio(Portfolio portfolio);

    @Query(nativeQuery = true, value="select t.exchange_id as ExchangeId, t.exchange_name as ExchangeName, " +
            "o.ticker_label as Ticker, t.quantity as Quantity, t.price as Price, o.side as Side, o.created_at as CreatedAt, " +
            "o.order_type as OrderType from client_order o join trade t on t.order_order_id = o.order_id where o.portfolio_id = ?1 and t.status = 'CREATED' " +
            "order by o.created_at desc")
    List<FetchAllClientOrdersByPortfolioWherePendingResponse> findAllByPortfolioWhereStatusCreated(Long portfolioId);
}

