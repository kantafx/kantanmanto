package com.kantafx.order.repositories;

import com.kantafx.order.models.Execution;
import com.kantafx.order.models.Trade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface ExecutionRepository extends JpaRepository<Execution, Timestamp> {
    List<Execution> findAllByTrade(Optional<Trade> trade);
}
