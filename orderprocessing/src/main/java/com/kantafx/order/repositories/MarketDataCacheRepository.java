package com.kantafx.order.repositories;

import com.kantafx.order.payloads.TimestampedMarketData;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;

@Repository
public class MarketDataCacheRepository {
    public static TimestampedMarketData currentMarketData = new TimestampedMarketData(new ArrayList<>(), new Timestamp(new Date().getTime()));

    public static void setCurrentMarketData(TimestampedMarketData marketData) {
        currentMarketData = marketData;
    }

}
