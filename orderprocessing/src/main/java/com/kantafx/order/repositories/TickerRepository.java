package com.kantafx.order.repositories;

import com.kantafx.order.models.Ticker;
import com.kantafx.order.models.TickerLable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TickerRepository extends JpaRepository<Ticker, TickerLable> {
    @Query(value = "select * from ticker t where t.ticker_lable = ?1", nativeQuery = true)
    Optional<Ticker> findByTickerLabel (String tickerLable);
}
