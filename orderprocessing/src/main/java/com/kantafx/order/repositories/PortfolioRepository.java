package com.kantafx.order.repositories;

import com.kantafx.order.models.Client;
import com.kantafx.order.models.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {
    List<Portfolio> findAllByClient(Client client);

    Optional<Portfolio> findByClientAndIsDefault(Client client, boolean b);

    @Query(value = "select client_id from portfolio where portfolio_id = :portfolio", nativeQuery = true)
    Optional<Long> findClientIdByPortfolioId(Long portfolio);

    List<Portfolio> findAllByClientOrderByCreatedAtAsc(Client client);
}
