package com.kantafx.order.repositories;

import com.kantafx.order.models.Portfolio;
import com.kantafx.order.models.Stock;
import com.kantafx.order.models.Ticker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
    Optional<Stock> findByTickerAndPortfolio(Ticker ticker, Portfolio portfolio);

    List<Stock> findAllByPortfolio(Portfolio portfolio);
}
