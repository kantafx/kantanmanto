package com.kantafx.order.repositories;

import com.kantafx.order.models.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdministratorRepository extends JpaRepository<Administrator, String> {
    Optional<Administrator> findByEmail(String email);
}
