package com.kantafx.order.controllers;

import com.kantafx.order.payloads.AdminDetailsResponse;
import com.kantafx.order.payloads.ClientDetailsResponse;
import com.kantafx.order.payloads.LoginRequest;
import com.kantafx.order.payloads.ResponseBuilder;
import com.kantafx.order.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/admin/login")
    public ResponseEntity verifyAdministrator(@RequestBody LoginRequest request)
    {
        try{
            AdminDetailsResponse response = authenticationService.verifyAdministrator(request);
            return ResponseEntity.ok(response);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @PostMapping("/client/login")
    public ResponseEntity verifyClient(@RequestBody LoginRequest request)
    {
        try{
            ClientDetailsResponse response = authenticationService.verifyClient(request);
            return ResponseEntity.ok(response);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }
}
