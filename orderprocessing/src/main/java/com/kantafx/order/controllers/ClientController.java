package com.kantafx.order.controllers;

import com.kantafx.order.models.ClientOrder;
import com.kantafx.order.models.Portfolio;
import com.kantafx.order.models.Stock;
import com.kantafx.order.models.Ticker;
import com.kantafx.order.payloads.*;
import com.kantafx.order.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClientController {
    @Autowired
    private ClientService clientService;

    @PostMapping("/client/register")
    public ResponseEntity saveClient(@RequestBody ClientRegistrationRequest request){
        try{
            ClientDetailsResponse details = clientService.saveClient(request);
            return ResponseEntity.ok(details);
        }
        catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @PostMapping("/client/portfolio")
    public ResponseEntity savePortfolio(@RequestBody NewPortfolioRequest request)
    {
        try{
            clientService.createPortfolio(request);
            return ResponseEntity.ok(new ResponseBuilder(HttpStatus.CREATED,"Portfolio created successfully"));
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/portfolio/{clientId}")
    public ResponseEntity fetchAllPortfoliosByClient(@PathVariable(name = "clientId") Long clientId)
    {
        try{
            List<Portfolio> portfolios = clientService.fetchAllPortfoliosByClient(clientId);
            return ResponseEntity.ok(portfolios);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/portfolio/{clientId}/{portfolioId}")
    public ResponseEntity fetchClientPortfolioById(@PathVariable(name = "clientId") Long clientId, @PathVariable(name = "portfolioId") Long portfolioId)
    {
        try{
            Portfolio portfolio = clientService.fetchClientPortfolioById(clientId, portfolioId);
            return ResponseEntity.ok(portfolio);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @PostMapping("/client/order")
    public ResponseEntity saveOrder(@RequestBody ClientOrderRequest request) {
        try{
            String response = clientService.createOrder(request);
            return ResponseEntity.ok(new ResponseBuilder(HttpStatus.CREATED, response));
        }catch (Exception ex)
        {
            throw new RuntimeException(ex.getMessage());
           // return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @GetMapping("/client/orders/{clientId}")
    public ResponseEntity fetchAllOrdersByClient(@PathVariable(name = "clientId") Long clientId)
    {
        try{
            List<FetchAllClientOrdersResponse> response = clientService.fetchAllOrdersByClient(clientId);
            return ResponseEntity.ok(response);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/{portfolioId}/orders")
    public ResponseEntity fetchAllOrdersByPortfolio(@PathVariable(name = "portfolioId") Long portfolioId)
    {
        try{
            List<ClientOrder> response = clientService.fetchAllOrdersByPortfolio(portfolioId);
            return ResponseEntity.ok(response);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/{portfolioId}/orders/pending")
    public ResponseEntity fetchAllOrdersByPortfolioWhereOrderPending(@PathVariable(name = "portfolioId") Long portfolioId)
    {
        try{
            List<FetchAllClientOrdersByPortfolioWherePendingResponse> response = clientService.fetchAllOrdersByPortfolioWhereOrderPending(portfolioId);
            return ResponseEntity.ok(response);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @PostMapping("/client/stock")
    public ResponseEntity createStock(@RequestBody StockRequest request){
        try{
            Stock stock = clientService.createStock(request);
            return ResponseEntity.ok(stock);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @DeleteMapping("/client/order/{exchangeOrderId}")
    public ResponseEntity cancelOrder(@PathVariable String exchangeOrderId)
    {
        try{
            clientService.cancelOrder(exchangeOrderId);
            return ResponseEntity.ok(new ResponseBuilder(HttpStatus.CREATED, "Order deleted successfully, amount refunded to portfolio"));
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/tickers")
    public List<Ticker> fetchAllTickers()
    {
        return clientService.fetchAllTickers();
    }

    @DeleteMapping("/client/portfolio/{portfolioId}")
    public ResponseEntity closePortfolio(@PathVariable Long portfolioId){
        try{
            clientService.closePortfolio(portfolioId);
            return ResponseEntity.ok(new ResponseBuilder(HttpStatus.CREATED, "Portfolio deleted successfully, orders and stocks added to default portfolio"));
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }

    }

    @GetMapping("/client/stock/{portfolioId}")
    public ResponseEntity fetchAllClientStock(@PathVariable Long portfolioId)
    {
        try{
            List<FetchAllStockResponse> stocks = clientService.fetchAllClientStock(portfolioId);
            return ResponseEntity.ok(stocks);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }

    @GetMapping("/client/prices")
    public ResponseEntity getCurrentTickerPrices()
    {
        try{
            List<CurrentPricesPayload> payload = clientService.getCurrentTickerPrices();
            return ResponseEntity.ok(payload);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }


    @GetMapping("/client/executions/{tradeId}")
    public ResponseEntity getAllExecutionsOnTrade(@PathVariable String tradeId)
    {
        try{
            List<FetchExecutionsResponse> payload = clientService.getAllExecutionsOnTrade(tradeId);
            return ResponseEntity.ok(payload);
        }catch (Exception ex)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseBuilder(HttpStatus.BAD_REQUEST, ex.getMessage()));
        }
    }
}
