package com.kantafx.order.controllers;

import com.kantafx.order.models.Ticker;
import com.kantafx.order.services.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @PostMapping("/admin/ticker")
    public Ticker saveTicker(@RequestBody Ticker request){
        return administratorService.saveTicker(request);
    }

    @GetMapping("admin/ticker")
    public List<Ticker> fetchAllTickers(){
        return administratorService.fetchAllTickers();
    }

}
