package com.kantafx.order.services;

import com.kantafx.order.exception.ResourceAlreadyExistsException;
import com.kantafx.order.exception.ResourceCouldNotBeCreatedException;
import com.kantafx.order.exception.ResourceNotFoundException;
import com.kantafx.order.exception.UnprocessableRequestException;
import com.kantafx.order.models.*;
import com.kantafx.order.payloads.ClientOrderRequest;
import com.kantafx.order.payloads.ClientRegistrationRequest;
import com.kantafx.order.payloads.NewPortfolioRequest;
import com.kantafx.order.payloads.StockRequest;
import com.kantafx.order.repositories.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;

@SpringBootTest
class ClientServiceTest {

    @Autowired
    private ClientService clientService;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private TickerRepository tickerRepository;

    @MockBean
    private PortfolioRepository portfolioRepository;

    @MockBean
    private ClientOrderRepository clientOrderRepository;

    @MockBean
    private OrderValidationService orderValidationService;

    @MockBean
    private OrderProcessingService orderProcessingService;

    @MockBean
    private StockRepository stockRepository;

    @MockBean
    private TradeRepository tradeRepository;

    @MockBean
    private WebClientService webClientService;


    //    MethodName_StateUnderTest_ExpectedBehavior
    @Test
    void saveClient_uniqueEmail_clientPersistedInDbAndDefaultPortfolioBuilt() throws Exception {
        //given a client registration request
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "kwamesarfo@turntabl.io",
                "Kwame Sarfo",
                "kwame"
        );

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findByEmail(request.email());
        Mockito.doReturn(new Client(1l, "email@email.com", "name", "password")).when(clientRepository).save(any());
        Mockito.doReturn(new Portfolio()).when(portfolioRepository).save(any());

        clientService.saveClient(request);

        //then
        Mockito.verify(clientRepository, Mockito.times(1)).save(any());
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(any());

    }

    @Test
    void saveClient_uniqueEmail_throwsResourceCouldNotBeCreatedException_whenDefaultPortfolioCannotBeCreated() {
        //given a client registration request
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "kwamesarfo@turntabl.io",
                "Kwame Sarfo",
                "kwame"
        );

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findByEmail(request.email());
        Mockito.doReturn(new Client()).when(clientRepository).save(any());
        Mockito.doReturn(null).when(portfolioRepository).save(any());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.saveClient(request));
        Mockito.verify(clientRepository, Mockito.times(1)).save(any(Client.class));
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(any(Portfolio.class));
        Mockito.verify(clientRepository, Mockito.times(1)).delete(any());

    }

    @Test
    void saveClient_existingEmail_throwsResourceAlreadyExistsException() {
        //given a client registration request
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "kwamesarfo@turntabl.io",
                "Kwame Sarfo",
                "kwame"
        );

        //when
        Mockito.doReturn(Optional.of(new Client())).when(clientRepository).findByEmail(request.email());

        //then
        Assertions.assertThrows(ResourceAlreadyExistsException.class, () -> clientService.saveClient(request));
        Mockito.verify(clientRepository, Mockito.times(0)).save(any(Client.class));
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any(Portfolio.class));

    }

    @Test
    void createPortfolio_validClientEmail_portfolioPersistedInDb() throws Exception {
        //given a portfolio creation request
        NewPortfolioRequest request = new NewPortfolioRequest(
                "kwamesarfo@turntabl.io",
                "portfolioName",
                1000d
        );

        //when
        Mockito.doReturn(Optional.of(new Client(1l, "email@email.com", "name", "password"))).when(clientRepository).findByEmail(request.email());
        Mockito.doReturn(new Portfolio()).when(portfolioRepository).save(any());

        clientService.createPortfolio(request);


        //then
        Mockito.verify(clientRepository, Mockito.times(1)).findByEmail(request.email());
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(any());
    }

    @Test
    void createPortfolio_invalidClientEmail_throwsResourceCouldNotBeCreatedException_whenPortfolioCannotBePersistedInDb(){
        //given a portfolio creation request
        NewPortfolioRequest request = new NewPortfolioRequest(
                "kwamesarfo@turntabl.io",
                "portfolioName",
                1000d
        );

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findByEmail(request.email());


        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.createPortfolio(request));
        Mockito.verify(clientRepository, Mockito.times(1)).findByEmail(request.email());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());
    }


    @Test
    void createPortfolio_invalidClientEmail_throwsResourceNotFoundException() throws Exception {
        //given a portfolio creation request
        NewPortfolioRequest request = new NewPortfolioRequest(
                "kwamesarfo@turntabl.io",
                "portfolioName",
                1000d
        );

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findByEmail(request.email());


        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.createPortfolio(request));
        Mockito.verify(clientRepository, Mockito.times(1)).findByEmail(request.email());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());
    }



    @Test
    void fetchAllPortfoliosByClient_validClientId_listOfClientPortfolios() throws ResourceNotFoundException {
        //given a client
        Long clientId = 1l;

        //when
        Mockito.doReturn(Optional.of(new Client(1l, "email@email.com", "name", "password"))).when(clientRepository).findById(clientId);
        Mockito.doReturn(List.of(new Portfolio(), new Portfolio())).when(portfolioRepository).findAllByClientOrderByCreatedAtAsc(any());

        clientService.fetchAllPortfoliosByClient(clientId);

        //then
        Mockito.verify(clientRepository, Mockito.times(1)).findById(clientId);
        Mockito.verify(portfolioRepository, Mockito.times(1)).findAllByClientOrderByCreatedAtAsc(any(Client.class));
    }

    @Test
    void fetchAllPortfoliosByClient_invalidClientId_throwsResourceNotFoundException()
    {
        //given a client
        Long clientId = 1l;

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findById(clientId);

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.fetchAllPortfoliosByClient(clientId));
        Mockito.verify(clientRepository, Mockito.times(1)).findById(clientId);
        Mockito.verify(portfolioRepository, Mockito.times(0)).findAllByClient(any(Client.class));
    }

    @Test
    void createOrder_validMarketOrder_validatedAndProcessed() throws Exception {
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                4,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );


        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());
        Mockito.doReturn(List.of(new MarketData(), new MarketData())).when(orderValidationService).getValidExchangesForOrder(any(ClientOrder.class));

        clientService.createOrder(request);

        //then
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(1)).getValidExchangesForOrder(any(ClientOrder.class));
        Mockito.verify(orderProcessingService, Mockito.times(1)).processOrder(any(ClientOrder.class), any());
    }


    @Test
    void createOrder_validLimitOrder_validatedAndProcessed() throws Exception {
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                4,
                2.50,
                Side.BUY,
                OrderType.LIMIT
        );


        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());
        Mockito.doReturn(List.of(new MarketData(), new MarketData())).when(orderValidationService).getValidExchangesForOrder(any(ClientOrder.class));

        clientService.createOrder(request);

        //then
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(1)).getValidExchangesForOrder(any(ClientOrder.class));
        Mockito.verify(orderProcessingService, Mockito.times(1)).processOrder(any(ClientOrder.class), any());
    }


    @Test
    void createOrder_invalidOrderQuantity_throwsResourcesCouldNotBeCreatedException() throws Exception{
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                0,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );


        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createOrder(request));
        Mockito.verify(tickerRepository, Mockito.times(0)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(0)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(0)).getValidExchangesForOrder(any(ClientOrder.class));
        Mockito.verify(orderProcessingService, Mockito.times(0)).processOrder(any(ClientOrder.class), any());
    }


    @Test
    void createOrder_invalidTicker_throwsResourcesCouldNotBeCreatedException() throws Exception{
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                4,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );


        //when
        Mockito.doReturn(Optional.empty()).when(tickerRepository).findByTickerLabel(any());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createOrder(request));
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(0)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(0)).getValidExchangesForOrder(any(ClientOrder.class));
        Mockito.verify(orderProcessingService, Mockito.times(0)).processOrder(any(ClientOrder.class), any());
    }

    @Test
    void createOrder_invalidPortfolio_throwsResourcesCouldNotBeCreatedException() throws Exception{
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                4,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );


        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findById(request.portfolioId());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createOrder(request));
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(0)).getValidExchangesForOrder(any(ClientOrder.class));
        Mockito.verify(orderProcessingService, Mockito.times(0)).processOrder(any(ClientOrder.class), any());
    }


    @Test
    void createOrder_noValidExchangeFound_throwsResourcesCouldNotBeCreatedException() throws Exception{
        //given an order request
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                4,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );


        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());
        Mockito.doReturn(new ArrayList<>()).when(orderValidationService).getValidExchangesForOrder(any());

        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createOrder(request));
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(orderValidationService, Mockito.times(1)).getValidExchangesForOrder(any());
        Mockito.verify(orderProcessingService, Mockito.times(0)).processOrder(any(), any());
    }


    @Test
    void createStock_validTickerAndPortfolio_stockPersistedInDb() throws Exception {
        //given a portfolio creation request
        StockRequest request = new StockRequest(
                TickerLable.AAPL,
                1l,
                5,
                1000d
        );

        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());

        clientService.createStock(request);


        //then
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(stockRepository, Mockito.times(1)).save(any());
    }


    @Test
    void createStock_invalidTicker_throwsResourceCouldNotBeCreatedException(){
        //given a portfolio creation request
        StockRequest request = new StockRequest(
                TickerLable.AAPL,
                1l,
                5,
                1000d
        );

        //when
        Mockito.doReturn(Optional.empty()).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(request.portfolioId());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createStock(request));
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(0)).findById(request.portfolioId());
        Mockito.verify(stockRepository, Mockito.times(0)).save(any());
    }


    @Test
    void createStock_invalidPortfolio_throwsResourceCouldNotBeCreatedException(){
        //given a portfolio creation request
        StockRequest request = new StockRequest(
                TickerLable.AAPL,
                1l,
                5,
                1000d
        );

        //when
        Mockito.doReturn(Optional.of(new Ticker())).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findById(request.portfolioId());


        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, () -> clientService.createStock(request));
        Mockito.verify(tickerRepository, Mockito.times(1)).findByTickerLabel(request.tickerLable().toString());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(request.portfolioId());
        Mockito.verify(stockRepository, Mockito.times(0)).save(any());
    }


//    @Test
//    public void shouldVerifyMarketOrderHasZeroPriceValue(){
//        //given an order request
//        ClientOrderRequest request = new ClientOrderRequest(
//                TickerLable.AAPL,
//                1l,
//                4,
//                2.50,
//                Side.BUY,
//                OrderType.MARKET
//        );
//
//        //create ticker
//        Ticker ticker = new Ticker(TickerLable.AAPL);
//
//        //create portfolio
//        Portfolio portfolio = new Portfolio(1l, new Client(), "portfolio1", 1000d, new ArrayList<>());
//
//
//    }

  //  @Test
//    public void shouldNotVerifyOrderTypeValidationWasCalledWhenOrderTypeLimit() {
////        arrange
//        ClientOrderRequest request = new ClientOrderRequest(
//                TickerLable.AAPL,
//                1l,
//                4,
//                2.50,
//                Side.BUY,
//                OrderType.LIMIT
//        );
//
//        //create ticker
//        Ticker ticker = new Ticker(TickerLable.AAPL, "stock for Apple companies");
//
//        //create portfolio
//        Portfolio portfolio = new Portfolio(1l, new Client(), false, "portfolio1", 1000d,Timestamp.from(Instant.now()), new ArrayList<>());
//
//
//        //when
//        Mockito.doReturn(Optional.of(ticker)).when(tickerRepository).findByTickerLabel(request.tickerLable().toString());
//        Mockito.doReturn(Optional.of(portfolio)).when(portfolioRepository).findById(request.portfolioId());
//        Mockito.doReturn(new ClientOrder()).when(clientOrderRepository).save(any());
//        clientService.createOrder(request);
//
//
////        assert
//        //verify(orderValidationService, times(1)).resetPriceIfMarketOrder(Mockito.any());
//    }


    @Test
    void cancelOrder_singleLegOrderValidExchangeOrderId_orderCancelled() throws ResourceNotFoundException {
        //given an exchange Order Id
        String exchangeOrderId = "aaaged-dfddfe-erfaere-dggfvh";

        //exchange endpoint
        String exchangeEndpoint = "http://exchange.com";

        //portfolio
        Portfolio portfolio = Portfolio.builder()
                .amount(1000d)
                .build();
        //order
        ClientOrder order = ClientOrder.builder()
                .portfolio(portfolio)
                .status(Status.CREATED)
                .build();
        //trade
        Trade trade = Trade.builder()
                .quantity(10)
                .price(10d)
                .order(order)
                .status(Status.CREATED)
                .build();

        //all trades in order
        List<Trade> tradesInOrder = List.of(
                trade
        );

        //put trades in order
        order.setTrades(tradesInOrder);

        //when
        Mockito.doReturn(Optional.of(trade)).when(tradeRepository).findById(any());
        Mockito.doReturn(exchangeEndpoint).when(webClientService).queryMarketDataForExchangeEndpoint(any());

        clientService.cancelOrder(exchangeOrderId);

        //then
        Mockito.verify(tradeRepository, Mockito.times(1)).findById(exchangeOrderId);
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());
        Mockito.verify(clientOrderRepository, Mockito.times(1)).save(any());
        Mockito.verify(webClientService, Mockito.times(1)).sendCancellationRequestToExchange(exchangeEndpoint, exchangeOrderId);

        //status is set on trade
        Assertions.assertEquals(Status.CANCELLED, trade.getStatus());

        //order updatedAt time not null since a leg was cancelled
        Assertions.assertNotNull(order.getUpdatedAt());

        //assert order status cancelled since it's a single leg order
        Assertions.assertEquals(Status.CANCELLED, order.getStatus());

        //portfolio balance updated
        Assertions.assertEquals(1100d, portfolio.getAmount());
    }

    @Test
    void cancelOrder_multilegOrderValidExchangeOrderId_orderLegCancelled() throws ResourceNotFoundException {
        //given an exchange Order Id
        String exchangeOrderId = "aaaged-dfddfe-erfaere-dggfvh";

        //exchange endpoint
        String exchangeEndpoint = "http://exchange.com";

        //portfolio
        Portfolio portfolio = Portfolio.builder()
                .amount(1000d)
                .build();
        //order
        ClientOrder order = ClientOrder.builder()
                .portfolio(portfolio)
                .trades(new ArrayList<>())
                .status(Status.CREATED)
                .build();
        //trade
        Trade trade = Trade.builder()
                .quantity(10)
                .price(10d)
                .order(order)
                .status(Status.CREATED)
                .build();

        //all trades in order
        List<Trade> tradesInOrder = List.of(
                Trade.builder()
                        .status(Status.CREATED)
                        .build(),
                trade
        );

        //put trades in order
        order.builder().trades(tradesInOrder).build();

        //when
        Mockito.doReturn(Optional.of(trade)).when(tradeRepository).findById(any());
        Mockito.doReturn(exchangeEndpoint).when(webClientService).queryMarketDataForExchangeEndpoint(any());
        clientService.cancelOrder(exchangeOrderId);

        //then
        Mockito.verify(tradeRepository, Mockito.times(1)).findById(exchangeOrderId);
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());
        Mockito.verify(clientOrderRepository, Mockito.times(1)).save(any());
        Mockito.verify(webClientService, Mockito.times(1)).sendCancellationRequestToExchange(exchangeEndpoint, exchangeOrderId);

        //status is set
        Assertions.assertEquals(Status.CANCELLED, trade.getStatus());

        //order updatedAt time not null since a leg was cancelled
        Assertions.assertNotNull(order.getUpdatedAt());

        //assert order status not cancelled since only one leg of order cancelled
        Assertions.assertEquals(Status.CREATED, order.getStatus());

        //portfolio balance updated
        Assertions.assertEquals(1100d, portfolio.getAmount());
    }


    @Test
    void cancelOrder_multilegOrderValidExchangeOrderIdAllLegsCancelled_orderCancelled() throws ResourceNotFoundException {
        //given an exchange Order Id
        String exchangeOrderId = "aaaged-dfddfe-erfaere-dggfvh";

        //exchange endpoint
        String exchangeEndpoint = "http://exchange.com";

        //portfolio
        Portfolio portfolio = Portfolio.builder()
                .amount(1000d)
                .build();
        //order
        ClientOrder order = ClientOrder.builder()
                .portfolio(portfolio)
                .status(Status.CREATED)
                .build();
        //trade
        Trade trade1 = Trade.builder()
                .quantity(10)
                .price(10d)
                .order(order)
                .status(Status.CREATED)
                .build();

        Trade trade2 = Trade.builder()
                .status(Status.CANCELLED)
                .build();

        //all trades in order
        List<Trade> tradesInOrder = List.of(
                trade1,
                trade2
        );

        //put trades in order
        order.setTrades(tradesInOrder);

        //when
        Mockito.doReturn(Optional.of(trade1)).when(tradeRepository).findById(any());
        Mockito.doReturn(exchangeEndpoint).when(webClientService).queryMarketDataForExchangeEndpoint(any());
        clientService.cancelOrder(exchangeOrderId);

        //then
        Mockito.verify(tradeRepository, Mockito.times(1)).findById(exchangeOrderId);
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());
        Mockito.verify(clientOrderRepository, Mockito.times(1)).save(any());
        Mockito.verify(webClientService, Mockito.times(1)).sendCancellationRequestToExchange(exchangeEndpoint, exchangeOrderId);

        //status is set
        Assertions.assertEquals(Status.CANCELLED, trade1.getStatus());

        //order updatedAt time not null since a leg was cancelled
        Assertions.assertNotNull(order.getUpdatedAt());

        //assert order status cancelled since all legs of order cancelled
        Assertions.assertEquals(Status.CANCELLED, order.getStatus());

        //portfolio balance updated
        Assertions.assertEquals(1100d, portfolio.getAmount());
    }


    @Test
    void cancelOrder_inValidExchangeOrderId_throwsResourceNotFoundException() {
        //given an exchange Order Id
        String exchangeOrderId = "aaaged-dfddfe-erfaere-dggfvh";

        //when
        Mockito.doReturn(Optional.empty()).when(tradeRepository).findById(any());


        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.cancelOrder(exchangeOrderId));
        Mockito.verify(tradeRepository, Mockito.times(1)).findById(exchangeOrderId);
        Mockito.verify(tradeRepository, Mockito.times(0)).save(any());
        Mockito.verify(clientOrderRepository, Mockito.times(0)).save(any());
    }

    @Test
    void fetchAllOrdersByClient_validClient_listOfOrders() throws ResourceNotFoundException {
        //given a client id
        Long clientId = 1l;

        //when
        Mockito.doReturn(Optional.of(new Client())).when(clientRepository).findById(clientId);
        clientService.fetchAllOrdersByClient(clientId);

        //then
        Mockito.verify(clientOrderRepository, Mockito.times(1)).findAllByClientId(clientId);
    }

    @Test
    void fetchAllOrdersByClient_inValidClient_ThrowsResourcesNotFoundException() {
        //given a client id
        Long clientId = 1l;

        //when
        Mockito.doReturn(Optional.empty()).when(clientRepository).findById(clientId);

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.fetchAllOrdersByClient(clientId));
        Mockito.verify(clientOrderRepository, Mockito.times(0)).findAllByClientId(clientId);
    }


    @Test
    void closePortfolio_validClosingPortfolioDetails_portfolioClosed() throws Exception {
        Client client = Client.builder()
                .clientId(1L)
                .email("joana@kantafx.com")
                .name("Joana")
                .password("password")
                .build();

        Portfolio defaultPortfolio =Portfolio.builder()
                .portfolioId(1L)
                .portfolioName("default")
                .isDefault(true)
                .amount(30.00)
                .client(client)
                .build();

        Portfolio closingPortfolio = Portfolio.builder()
                .portfolioId(2L)
                .portfolioName("portfolio name")
                .isDefault(false)
                .amount(30.00)
                .client(client)
                .stock(new ArrayList<>())
                .build();

        ClientOrder order1 = ClientOrder.builder()
                .portfolio(closingPortfolio)
                .build();

        ClientOrder order2 = ClientOrder.builder()
                .portfolio(closingPortfolio)
                .build();

        List<ClientOrder> ordersInPortfolio = List.of(
                order1, order2
        );

        Mockito.doReturn(Optional.of(closingPortfolio)).when(portfolioRepository).findById(closingPortfolio.getPortfolioId());
        Mockito.doReturn(Optional.of(defaultPortfolio)).when(portfolioRepository).findByClientAndIsDefault(client, true);
        Mockito.doReturn(new ArrayList<>()).when(stockRepository).findAllByPortfolio(closingPortfolio);
        Mockito.doReturn(ordersInPortfolio).when(clientOrderRepository).findAllByPortfolio(closingPortfolio);


        clientService.closePortfolio(2L);

        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(closingPortfolio.getPortfolioId());
        Mockito.verify(portfolioRepository, Mockito.times(1)).findByClientAndIsDefault(client, true);
        Mockito.verify(portfolioRepository, Mockito.times(1)).findByClientAndIsDefault(client, true);
        Mockito.verify(stockRepository, Mockito.times(1)).findAllByPortfolio(closingPortfolio);

        //verify default portfolio balance updated
        Assertions.assertEquals(60.00, defaultPortfolio.getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(defaultPortfolio);

        //verify all orders are pointed to default portfolio
        Assertions.assertEquals(order1.getPortfolio(), defaultPortfolio);
        Assertions.assertEquals(order2.getPortfolio(), defaultPortfolio);
        Mockito.verify(clientOrderRepository, Mockito.times(1)).saveAll(ordersInPortfolio);

        //verify portfolio is deleted
        Mockito.verify(portfolioRepository, Mockito.times(1)).delete(closingPortfolio);
    }

    @Test
    void closePortfolio_invalidClosingPortfolioDetails_throwsResourceNotFoundException() {
        //given an invalid portfolio id
        Long invalidId = 1l;

        //when
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findById(invalidId);

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.closePortfolio(invalidId));

        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(invalidId);
        Mockito.verify(portfolioRepository, Mockito.times(0)).delete(any());
    }

    @Test
    void closePortfolio_defaultPortfolioIdProvided_throwsUnprocessableRequestException() {
        //given an invalid portfolio id
        Long defaultPortfolioId = 1l;

        Portfolio defaultPortfolio = Portfolio.builder()
                .isDefault(true)
                .build();

        //when
        Mockito.doReturn(Optional.of(defaultPortfolio)).when(portfolioRepository).findById(defaultPortfolioId);

        //then
        Assertions.assertThrows(UnprocessableRequestException.class, () -> clientService.closePortfolio(defaultPortfolioId));

        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(defaultPortfolioId);
        Mockito.verify(portfolioRepository, Mockito.times(0)).delete(any());
    }

    @Test
    void closePortfolio_validClosingPortfolioDetails_throwsResourceNotFoundException_whenDefaultPortfolioNotFound() {
        //given an invalid portfolio id
        Long portfolioId = 1l;

        Portfolio portfolio = Portfolio.builder()
                .isDefault(false)
                .build();
        //when
        Mockito.doReturn(Optional.of(portfolio)).when(portfolioRepository).findById(portfolioId);
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findByClientAndIsDefault(any(), anyBoolean());

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, () -> clientService.closePortfolio(portfolioId));

        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(portfolioRepository, Mockito.times(0)).delete(any());
    }

    @Test
    void closePortfolio_validClosingPortfolioDetails_throwsUnprocessableRequestException_whenClosingPortfolioHasStocks() {
        //given an invalid portfolio id
        Long portfolioId = 1l;

        Portfolio portfolio = Portfolio.builder()
                .isDefault(false)
                .build();

        Portfolio defaultPortfolio = Portfolio.builder()
                .isDefault(true)
                .build();

        List<Stock> portfolioStocks = List.of(new Stock(), new Stock());

        //when
        Mockito.doReturn(Optional.of(portfolio)).when(portfolioRepository).findById(portfolioId);
        Mockito.doReturn(Optional.of(defaultPortfolio)).when(portfolioRepository).findByClientAndIsDefault(any(), anyBoolean());
        Mockito.doReturn(portfolioStocks).when(stockRepository).findAllByPortfolio(any());

        //then
        Assertions.assertThrows(UnprocessableRequestException.class, () -> clientService.closePortfolio(portfolioId));

        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(portfolioRepository, Mockito.times(0)).delete(any());
    }


    @Test
    void fetchAllTickers() {
        clientService.fetchAllTickers();

        Mockito.verify(tickerRepository, Mockito.times(1)).findAll();
    }


    @Test
    void fetchAllOrdersByPortfolio_validPortfolioDetails_listOrders() throws ResourceNotFoundException {
        //given a valid portfolio id
        Long portfolioId = 1l;


        //when
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(portfolioId);
        clientService.fetchAllOrdersByPortfolio(portfolioId);

        //then
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(clientOrderRepository, Mockito.times(1)).findAllByPortfolio(any());
    }

    @Test
    void fetchAllOrdersByPortfolio_invalidPortfolioDetails_throwResourceNotFoundException() {
        //given an invalid portfolio id
        Long portfolioId = 1l;

        //when
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findById(portfolioId);

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, ()->clientService.fetchAllOrdersByPortfolio(portfolioId));
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(clientOrderRepository, Mockito.times(0)).findAllByPortfolio(any());
    }

    @Test
    void fetchAllOrdersByPortfolioWhereOrderPending_validPortfolioDetails_listOrders() throws ResourceNotFoundException {
        //given a valid portfolio id
        Long portfolioId = 1l;


        //when
        Mockito.doReturn(Optional.of(new Portfolio())).when(portfolioRepository).findById(portfolioId);
        clientService.fetchAllOrdersByPortfolioWhereOrderPending(portfolioId);

        //then
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(clientOrderRepository, Mockito.times(1)).findAllByPortfolioWhereStatusCreated(any());
    }

    @Test
    void fetchAllOrdersByPortfolioWhereOrderPending_invalidPortfolioDetails_throwResourceNotFoundException() {
        //given an invalid portfolio id
        Long portfolioId = 1l;

        //when
        Mockito.doReturn(Optional.empty()).when(portfolioRepository).findById(portfolioId);

        //then
        Assertions.assertThrows(ResourceNotFoundException.class, ()->clientService.fetchAllOrdersByPortfolioWhereOrderPending(portfolioId));
        Mockito.verify(portfolioRepository, Mockito.times(1)).findById(portfolioId);
        Mockito.verify(clientOrderRepository, Mockito.times(0)).findAllByPortfolioWhereStatusCreated(any());
    }
}