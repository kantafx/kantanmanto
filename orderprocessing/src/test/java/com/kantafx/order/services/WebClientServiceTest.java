package com.kantafx.order.services;

import com.kantafx.order.models.ExchangeOrderRequest;
import com.kantafx.order.models.OrderType;
import com.kantafx.order.models.Side;
import com.kantafx.order.models.TickerLable;
import com.kantafx.order.payloads.MarketDataPayload;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@SpringBootTest
class WebClientServiceTest {

    @Autowired
    private WebClientService webClientService;

    @MockBean
    private WebClient webClient;

    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriSpecMock;

    @Mock
    private WebClient.RequestBodyUriSpec requestBodyUriSpec;

    @Mock
    private WebClient.RequestBodySpec requestBodySpec;

    @Mock
    private WebClient.ResponseSpec responseSpecMock;

    @Mock
    private WebClient.RequestHeadersSpec requestHeadersSpec;



    @Value("${token}")
    public String TOKEN;

    @Value("${market-data-endpoint}")
    public String MARKET_DATA_ENDPOINT;

    @Test
    void queryMarketDataForExchangeEndpoint() {
        //given
        String exchangeName = "MAL1";
        String endpoint = "https://exchange.matraining.com";

        //when
        Mockito.when(webClient.get()).thenReturn(requestHeadersUriSpecMock);
        Mockito.when(requestHeadersUriSpecMock.uri(ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        Mockito.when(requestHeadersSpec.retrieve()).thenReturn(responseSpecMock);
        Mockito.when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>notNull()))
                .thenReturn(Mono.just(endpoint)); webClientService.queryMarketDataForExchangeEndpoint(exchangeName);


        //then
        Mockito.verify(webClient, Mockito.times(1)).get();
    }

    @Test
    void sendCancellationRequestToExchange() {
        //given
        String endpoint = "https://exchange.matraining.com";
        String exchangeOrderId = "assdfd-dg45343-dsad-44334d";


        //when
        Mockito.when(webClient.delete()).thenReturn(requestHeadersUriSpecMock);
        Mockito.when(requestHeadersUriSpecMock.uri(ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        Mockito.when(requestHeadersSpec.retrieve()).thenReturn(responseSpecMock);
        Mockito.when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>notNull()))
                .thenReturn(Mono.just(exchangeOrderId));


        webClientService.sendCancellationRequestToExchange(endpoint, exchangeOrderId);

        //then
        Mockito.verify(webClient, Mockito.times(1)).delete();

    }

    @Test
    void getAvailableMarketPricesFromRemote() {
        //given
        MarketDataPayload[] payload = new MarketDataPayload[5];

        //when
        Mockito.when(webClient.get()).thenReturn(requestHeadersUriSpecMock);
        Mockito.when(requestHeadersUriSpecMock.uri(ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        Mockito.when(requestHeadersSpec.retrieve()).thenReturn(responseSpecMock);
        Mockito.when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<MarketDataPayload[]>>notNull()))
                .thenReturn(Mono.just(payload));
        webClientService.getAvailableMarketPricesFromRemote();

        //then
        Mockito.verify(webClient, Mockito.times(1)).get();
    }

    @Test
    void sendOrderToExchange() {
        //given
        String endpoint = "https://exchange.matraining.com";
        ExchangeOrderRequest request = new ExchangeOrderRequest(
                TickerLable.IBM, 4, 1.00, Side.BUY, OrderType.LIMIT
        );
        String exchangeOrderId = "assdfd-dg45343-dsad-44334d";

        //when
        Mockito.when(webClient.post()).thenReturn(requestBodyUriSpec);
        Mockito.when(requestBodyUriSpec.uri(ArgumentMatchers.anyString())).thenReturn(requestBodySpec);
        Mockito.when(requestBodySpec.header(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(requestBodySpec);
        Mockito.when(requestHeadersSpec.header(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(requestHeadersSpec);
        Mockito.when(requestBodySpec.accept(ArgumentMatchers.any())).thenReturn(requestBodySpec);
        Mockito.when(requestBodySpec.contentType(ArgumentMatchers.any())).thenReturn(requestBodySpec);
        Mockito.when(requestBodySpec.body(ArgumentMatchers.any())).thenReturn(requestHeadersSpec);
        Mockito.when(requestHeadersSpec.retrieve()).thenReturn(responseSpecMock);
        Mockito.when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>notNull()))
                .thenReturn(Mono.just(exchangeOrderId));
        webClientService.sendOrderToExchange(endpoint, request);

        //then
        Mockito.verify(webClient, Mockito.times(1)).post();

    }
}