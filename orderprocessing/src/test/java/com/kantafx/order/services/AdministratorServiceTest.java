package com.kantafx.order.services;

import com.kantafx.order.models.Ticker;
import com.kantafx.order.models.TickerLable;
import com.kantafx.order.repositories.TickerRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class AdministratorServiceTest {

    @Autowired
    private AdministratorService administratorService;

    @MockBean
    private TickerRepository tickerRepository;

    @Test
    void saveTicker_validTicker_tickerPersistedInDb() {
        //given
        Ticker ticker = new Ticker(TickerLable.AAPL, "stock for Apple companies");

        //when
        administratorService.saveTicker(ticker);

        //then
        Mockito.verify(tickerRepository, Mockito.times(1)).save(ticker);
    }

    @Test
    void fetchAllTickers() {
        //when
        administratorService.fetchAllTickers();

        //then
        Mockito.verify(tickerRepository, Mockito.times(1)).findAll();
    }
}