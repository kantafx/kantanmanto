package com.kantafx.order.services;

import com.kantafx.order.models.*;
import com.kantafx.order.repositories.PortfolioRepository;
import com.kantafx.order.repositories.StockRepository;
import com.kantafx.order.repositories.TickerRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class OrderValidationServiceTest {
    @Autowired
    private OrderValidationService orderValidationService;

    @MockBean
    private ClientService clientService;

    @MockBean
    private TickerRepository tickerRepository;

    @MockBean
    private PortfolioRepository portfolioRepository;


    @MockBean
    private StockRepository stockRepository;

    @MockBean
    private MarketDataService marketDataService;


    @Test
    public void validateOrderWithMarketData_validBuyLimitOrder_true(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertTrue(validity);
    }


    @Test
    public void validateOrderWithMarketData_invalidBuyLimitOrder_false_whenPriceAboveMaxPriceShift(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(23.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(22.00)
                .maxPriceShift(6.00)
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }


    @Test
    public void validateOrderWithMarketData_invalidBuyLimitOrder_false_whenBuyingFundsInadequate(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(10d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }

    @Test
    public void validateOrderWithMarketData_invalidBuyLimitOrder_false_whenBuyQuantityGreaterThanBuyLimit(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5000)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(2000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }



    //starts here
    @Test
    public void validateOrderWithMarketData_validSellLimitOrder_true(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        Stock stock = Stock.builder()
                .quantity(15)
                .build();

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.of(stock)).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertTrue(validity);
    }


    @Test
    public void validateOrderWithMarketData_invalidSellLimitOrder_false_whenPriceAboveMaxPriceShift(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(23.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(22.00)
                .maxPriceShift(6.00)
                .sellLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        Stock stock = Stock.builder()
                .quantity(15)
                .build();

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.of(stock)).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }


    @Test
    public void validateOrderWithMarketData_invalidSellLimitOrder_false_whenClientDoesNotOwnStock(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(10d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.empty()).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }

    @Test
    public void validateOrderWithMarketData_invalidSellLimitOrder_false_whenSellQuantityGreaterThanSellLimit(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5000)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.LIMIT).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(2000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        Stock stock = Stock.builder()
                .quantity(15)
                .build();

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.of(stock)).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);
    }



    // heerreeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    @Test
    public void validateOrderWithMarketData_validBuyMarketOrder_true(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertTrue(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }


    @Test
    public void validateOrderWithMarketData_invalidBuyMarketOrder_false_whenBuyingFundsInadequate(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(5d)
                        .build())
                .quantity(10)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }

    @Test
    public void validateOrderWithMarketData_invalidBuyMarketOrder_false_whenBuyQuantityGreaterThanBuyLimit(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5000)
                .price(3.00)
                .side(Side.BUY)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .buyLimit(2000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }



    //starts here
    @Test
    public void validateOrderWithMarketData_validSellMarketOrder_true(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        Stock stock = Stock.builder()
                .quantity(15)
                .build();

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.of(stock)).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertTrue(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }


    @Test
    public void validateOrderWithMarketData_invalidSellMarketOrder_false_whenClientDoesNotOwnStock(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(10d)
                        .build())
                .quantity(5)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.empty()).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }

    @Test
    public void validateOrderWithMarketData_invalidSellMarketOrder_false_whenSellQuantityGreaterThanSellLimit(){

        //        arrange
        ClientOrder order = ClientOrder.builder()
                .orderId(1L)
                .ticker(new Ticker())
                .portfolio(Portfolio.builder()
                        .amount(1000d)
                        .build())
                .quantity(5000)
                .price(3.00)
                .side(Side.SELL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .executedQuantity(0)
                .orderType(OrderType.MARKET).build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .sellLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .sellLimit(2000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        Stock stock = Stock.builder()
                .quantity(15)
                .build();

//        act
        Mockito.doReturn(marketData).when(marketDataService).getMarketData(order.getTicker());
        Mockito.doReturn(Optional.of(stock)).when(stockRepository).findByTickerAndPortfolio(any(), any());
        boolean validity = orderValidationService.validateOrderWithMarketData(order, marketData.get(0));


//        assert
        assertFalse(validity);

        //assert price is zero for market orders
        assertEquals(0.00, order.getPrice());
    }

}
