package com.kantafx.order.services;

import com.kantafx.order.exception.ResourceCouldNotBeCreatedException;
import com.kantafx.order.models.*;
import com.kantafx.order.repositories.ClientOrderRepository;
import com.kantafx.order.repositories.PortfolioRepository;
import com.kantafx.order.repositories.TradeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class OrderProcessingServiceTest {

    @MockBean
    private ClientOrderRepository clientOrderRepository;

    @MockBean
    private TradeRepository tradeRepository;

    @MockBean
    private PortfolioRepository portfolioRepository;

    @MockBean
    private WebClientService webClientService;

    @Autowired
    private OrderProcessingService orderProcessingService;

    @Test
    void processOrder_limitBuyOrderAcceptableOnAllExchanges_multiLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(8)
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .trades(new ArrayList<>())
                .price(3d)
                .side(Side.BUY)
                .orderType(OrderType.LIMIT)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange2.matraining.com")
                                .build()
                )
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);
        Trade trade2 = order.getTrades().get(1);

        //then
        Mockito.verify(webClientService, Mockito.times(2)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(2)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(3)).save(any());
        Assertions.assertEquals(2, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade2.getExchangeId());

        //assert that price used in limit order is price sent to exchange
        Assertions.assertEquals(3d, trade1.getPrice());
        Assertions.assertEquals(3d, trade2.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Multi-leg order created successfully"));

        //assert portfolio balance is updated
        Assertions.assertEquals(976, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(2)).save(any());

    }


    @Test
    void processOrder_limitSellOrderAcceptableOnAllExchanges_multiLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(8)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .price(3d)
                .side(Side.SELL)
                .orderType(OrderType.LIMIT)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange2.matraining.com")
                                .build()
                )
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);
        Trade trade2 = order.getTrades().get(1);

        //then
        Mockito.verify(webClientService, Mockito.times(2)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(2)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(3)).save(any());
        Assertions.assertEquals(2, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade2.getExchangeId());

        //assert that price used in limit order is price sent to exchange
        Assertions.assertEquals(3d, trade1.getPrice());
        Assertions.assertEquals(3d, trade2.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Multi-leg order created successfully"));

        //assert portfolio balance is not updated on sell order
        Assertions.assertEquals(1000d, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());

    }

    @Test
    void processOrder_limitBuyOrderAcceptableOnOneExchange_singleLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(7)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .price(3d)
                .side(Side.BUY)
                .orderType(OrderType.LIMIT)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();


        List<MarketData> marketData = List.of(exchangeOneMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);

        //then
        Mockito.verify(webClientService, Mockito.times(1)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(2)).save(any());
        Assertions.assertEquals(1, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());

        //assert that price used in limit order is price sent to exchange
        Assertions.assertEquals(3d, trade1.getPrice());


        Assertions.assertTrue(response.equalsIgnoreCase("Single-leg order created successfully"));

        //assert portfolio balance is updated
        Assertions.assertEquals(979, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(any());

    }


    @Test
    void processOrder_limitSellOrderAcceptableOnOneExchange_singleLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(7)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .price(3d)
                .side(Side.SELL)
                .orderType(OrderType.LIMIT)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();


        List<MarketData> marketData = List.of(exchangeOneMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);

        //then
        Mockito.verify(webClientService, Mockito.times(1)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(2)).save(any());
        Assertions.assertEquals(1, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());


        //assert that price used in limit order is price sent to exchange
        Assertions.assertEquals(3d, trade1.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Single-leg order created successfully"));

        //assert portfolio balance is not updated on sell order
        Assertions.assertEquals(1000d, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());

    }


    //herreeee
    @Test
    void processOrder_marketBuyOrderAcceptableOnAllExchanges_multiLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(8)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .price(3d)
                .side(Side.BUY)
                .orderType(OrderType.MARKET)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .askPrice(2.00)
                .maxPriceShift(6.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange2.matraining.com")
                                .build()
                )
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);
        Trade trade2 = order.getTrades().get(1);

        //then
        Mockito.verify(webClientService, Mockito.times(2)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(2)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(3)).save(any());
        Assertions.assertEquals(2, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade2.getExchangeId());

        //assert that price used in market order comes from exchange
        Assertions.assertEquals(1.00, trade1.getPrice());
        Assertions.assertEquals(2.00, trade2.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Multi-leg order created successfully"));

        //assert portfolio balance is updated (currently split even)
        Assertions.assertEquals(988, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(2)).save(any());

    }


    @Test
    void processOrder_marketSellOrderAcceptableOnAllExchanges_multiLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(8)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .price(3d)
                .side(Side.SELL)
                .orderType(OrderType.MARKET)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();

        MarketData exchangeTwoMd = MarketData.builder()
                .bidPrice(2.00)
                .maxPriceShift(6.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange2.matraining.com")
                                .build()
                )
                .buyLimit(5000)
                .build();

        List<MarketData> marketData = List.of(exchangeOneMd, exchangeTwoMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);
        Trade trade2 = order.getTrades().get(1);

        //then
        Mockito.verify(webClientService, Mockito.times(2)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(2)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(3)).save(any());
        Assertions.assertEquals(2, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade2.getExchangeId());

        //assert that price used in market order comes from exchange
        Assertions.assertEquals(1.00, trade1.getPrice());
        Assertions.assertEquals(2.00, trade2.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Multi-leg order created successfully"));

        //assert portfolio balance is not updated on sell order
        Assertions.assertEquals(1000d, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());

    }

    @Test
    void processOrder_marketBuyOrderAcceptableOnOneExchange_singleLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(7)
                .price(3d)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .side(Side.BUY)
                .orderType(OrderType.MARKET)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .askPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();


        List<MarketData> marketData = List.of(exchangeOneMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);

        //then
        Mockito.verify(webClientService, Mockito.times(1)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(2)).save(any());
        Assertions.assertEquals(1, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());

        //assert that price used in price used in market order comes from exchange
        Assertions.assertEquals(1.00, trade1.getPrice());


        Assertions.assertTrue(response.equalsIgnoreCase("Single-leg order created successfully"));

        //assert portfolio balance is updated
        Assertions.assertEquals(993, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(1)).save(any());

    }


    @Test
    void processOrder_marketSellOrderAcceptableOnOneExchange_singleLegOrder() throws ResourceCouldNotBeCreatedException {
        //given
        ClientOrder order = ClientOrder.builder()
                .portfolio(
                        Portfolio.builder()
                                .amount(1000d)
                                .build()
                )
                .quantity(7)
                .price(3d)
                .trades(new ArrayList<>())
                .ticker(Ticker.builder()
                        .tickerLable(TickerLable.AAPL)
                        .build())
                .side(Side.SELL)
                .orderType(OrderType.MARKET)
                .executedQuantity(0)
                .build();

        MarketData exchangeOneMd = MarketData.builder()
                .bidPrice(1.00)
                .maxPriceShift(5.00)
                .exchange(
                        Exchange.builder()
                                .endpoint("https:exchange.matraining.com")
                                .build()
                )
                .buyLimit(1000)
                .build();


        List<MarketData> marketData = List.of(exchangeOneMd);

        String exchangeId = "\"aeee433-readf434-aghhf54-adecvc\"";

        //when
        Mockito.doReturn(order).when(clientOrderRepository).save(any());
        Mockito.doReturn(exchangeId).when(webClientService).sendOrderToExchange(any(), any());
        String response = orderProcessingService.processOrder(order, marketData);


        //built trades
        Trade trade1 = order.getTrades().get(0);

        //then
        Mockito.verify(webClientService, Mockito.times(1)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(1)).save(any());

        //assert built trade is added to order
        Mockito.verify(clientOrderRepository, Mockito.times(2)).save(any());
        Assertions.assertEquals(1, order.getTrades().size());
        Assertions.assertEquals("aeee433-readf434-aghhf54-adecvc", trade1.getExchangeId());


        //assert that price used in market order comes from exchange
        Assertions.assertEquals(1.00, trade1.getPrice());

        Assertions.assertTrue(response.equalsIgnoreCase("Single-leg order created successfully"));

        //assert portfolio balance is not updated on sell order
        Assertions.assertEquals(1000d, order.getPortfolio().getAmount());
        Mockito.verify(portfolioRepository, Mockito.times(0)).save(any());

    }

    @Test
    void processOrder_invalidOrder_throwsResourceCouldNotBeCreatedException () {
        //when
        Mockito.doReturn(null).when(clientOrderRepository).save(any());

        //then
        Assertions.assertThrows(ResourceCouldNotBeCreatedException.class, ()->orderProcessingService.processOrder(new ClientOrder(), List.of()));
        Mockito.verify(webClientService, Mockito.times(0)).sendOrderToExchange(any(), any());
        Mockito.verify(tradeRepository, Mockito.times(0)).save(any());

    }
}