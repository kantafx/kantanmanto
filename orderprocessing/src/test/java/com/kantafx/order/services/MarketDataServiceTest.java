package com.kantafx.order.services;

import com.kantafx.order.models.Administrator;
import com.kantafx.order.models.Ticker;
import com.kantafx.order.models.TickerLable;
import com.kantafx.order.payloads.LoginRequest;
import com.kantafx.order.payloads.MarketDataMetaData;
import com.kantafx.order.payloads.MarketDataPayload;
import com.kantafx.order.repositories.MarketDataCacheRepository;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

@SpringBootTest
class MarketDataServiceTest {

    @MockBean
    private MarketDataCacheRepository marketDataCacheRepository;

    @MockBean
    private WebClientService webClientService;

    @Autowired
    private MarketDataService marketDataService;

    @Test
    void getMarketData_validCache_marketData() {
        //when
        MarketDataPayload marketDataPayload = MarketDataPayload.builder()
                .TICKER("AAPL")
                .build();

        Mockito.doReturn(new MarketDataMetaData(List.of(marketDataPayload), new Timestamp(new Date().getTime()))).when(marketDataCacheRepository).getCurrentMarketData();
        Mockito.doReturn(false).when(marketDataCacheRepository).marketDataCacheIsOutOfDate();

        Ticker ticker = Ticker.builder()
                .tickerLable(TickerLable.AAPL)
                .build();
        marketDataService.getMarketData(ticker);

        //then
        //assert new poll request not made
        Mockito.verify(webClientService, Mockito.times(0)).getAvailableMarketPricesFromRemote();
    }

    @Test
    void getMarketData_makesNewPollRequestWhenCacheIsEmpty_marketData() {

        //when
        Mockito.doReturn(new MarketDataMetaData(List.of(), new Timestamp(new Date().getTime()))).when(marketDataCacheRepository).getCurrentMarketData();
        Mockito.doReturn(false).when(marketDataCacheRepository).marketDataCacheIsOutOfDate();

        Ticker ticker = Ticker.builder()
                .tickerLable(TickerLable.AAPL)
                .build();
        marketDataService.getMarketData(ticker);

        //then
        //assert new poll request made
        Mockito.verify(webClientService, Mockito.times(1)).getAvailableMarketPricesFromRemote();
    }


    @Test
    void getMarketData_makesNewPollRequestWhenCacheIsOutOfDate_marketData() {

        MarketDataPayload marketDataPayload = MarketDataPayload.builder()
                .TICKER("AAPL")
                .build();

        Mockito.doReturn(new MarketDataMetaData(List.of(marketDataPayload), new Timestamp(new Date().getTime()))).when(marketDataCacheRepository).getCurrentMarketData();
        Mockito.doReturn(true).when(marketDataCacheRepository).marketDataCacheIsOutOfDate();

        Ticker ticker = Ticker.builder()
                .tickerLable(TickerLable.AAPL)
                .build();
        marketDataService.getMarketData(ticker);

        //then
        //assert new poll request made
        Mockito.verify(webClientService, Mockito.times(1)).getAvailableMarketPricesFromRemote();
    }
}