package com.kantafx.order.services;

import com.kantafx.order.exception.ResourceNotFoundException;
import com.kantafx.order.models.Administrator;
import com.kantafx.order.models.Client;
import com.kantafx.order.payloads.LoginRequest;
import com.kantafx.order.repositories.AdministratorRepository;
import com.kantafx.order.repositories.ClientRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AuthenticationServiceTest {
    @Autowired
    private AuthenticationService authenticationService;

    @MockBean
    private AdministratorRepository administratorRepository;

    @MockBean
    private ClientRepository clientRepository;



    @Test
    void verifyAdministrator_validCredentials_adminDetailsResponse() throws ResourceNotFoundException {
        //given
        Administrator administrator = new Administrator("tlc3@turntabl.io", "kantamanto");

        LoginRequest request = new LoginRequest("tlc3@turntabl.io", "kantamanto");

        //when
        Mockito.doReturn(Optional.of(administrator)).when(administratorRepository).findByEmail(request.email());

        authenticationService.verifyAdministrator(request);


        //then
        Mockito.verify(administratorRepository, Mockito.times(1)).findByEmail(request.email());

    }


    @Test
    void verifyAdministrator_invalidCredentials_throwsResourceNotFoundException() {
        //given
        LoginRequest request = new LoginRequest("tlc3@turntabl.io", "wrongpass");

        //when
        Mockito.doReturn(Optional.empty()).when(administratorRepository).findByEmail(request.email());


        //then
        Assertions.assertThrows(ResourceNotFoundException.class, ()-> authenticationService.verifyAdministrator(request));
        Mockito.verify(administratorRepository, Mockito.times(1)).findByEmail(request.email());
    }

    @Test
    void verifyClient_validCredentials_adminDetailsResponse() throws ResourceNotFoundException {
        //given a login request and a client
        LoginRequest request = new LoginRequest("joanateye@turntabl.io", "joana");

        Client client = Client.builder()
                .email("joanateye@turntabl.io")
                .password("joana")
                .build();
        //when
        Mockito.doReturn(Optional.of(client)).when(clientRepository).findByEmail(request.email());

        authenticationService.verifyClient(request);

        //then
        Mockito.verify(clientRepository, Mockito.times(1)).findByEmail(request.email());
    }

    @Test
    void verifyClient_invalidCredentials_throwsResourceNotFoundException() {
        //given a client

        LoginRequest request = new LoginRequest("robertayitey@turntabl.io", "rob");

        //when
        Mockito.doReturn(Optional.empty()).when(administratorRepository).findByEmail(request.email());


        //then
        Assertions.assertThrows(ResourceNotFoundException.class, ()-> authenticationService.verifyClient(request));
        Mockito.verify(clientRepository, Mockito.times(1)).findByEmail(request.email());

    }
}