package com.kantafx.order.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantafx.order.models.Ticker;
import com.kantafx.order.models.TickerLable;
import com.kantafx.order.services.AdministratorService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@AutoConfigureMockMvc
class AdministratorControllerTest {

    @MockBean
    private AdministratorService administratorService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper om;

    @Test
    void canSaveTicker() throws Exception {

        Ticker tickerJson = new Ticker(TickerLable.AAPL, "stock for Apple companies");


        Mockito.doReturn(tickerJson).when(administratorService).saveTicker(any());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/admin/ticker")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(tickerJson))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(administratorService, Mockito.times(1)).saveTicker(tickerJson);
    }

    @Test
    void doesNotSaveTickerWhenWrongEndpointUsed() throws Exception {

        Ticker tickerJson = new Ticker(TickerLable.AAPL, "stock for Apple companies");

        Mockito.doReturn(tickerJson).when(administratorService).saveTicker(any());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/admin/wrongticker")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(tickerJson))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(administratorService, Mockito.times(0)).saveTicker(tickerJson);
    }
}