package com.kantafx.order.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantafx.order.payloads.AdminDetailsResponse;
import com.kantafx.order.payloads.ClientDetailsResponse;
import com.kantafx.order.payloads.LoginRequest;
import com.kantafx.order.services.AuthenticationService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    @MockBean
    private AuthenticationService authenticationService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @Test
    void verifyAdministrator() throws Exception {
        LoginRequest request = new LoginRequest("test@email.com", "password");

        AdminDetailsResponse response = new AdminDetailsResponse("test@email.com");


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/admin/login")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //when
        Mockito.doReturn(response).when(authenticationService).verifyAdministrator(request);

        //then
        Mockito.verify(authenticationService, Mockito.times(1)).verifyAdministrator(request);

    }

    @Test
    void doesNotVerifyAdministratorAtWrongEndpoint() throws Exception {
        LoginRequest request = new LoginRequest("test@email.com", "password");

        AdminDetailsResponse response = new AdminDetailsResponse("test@email.com");


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/admin/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(authenticationService, Mockito.times(0)).verifyAdministrator(request);

    }

    @Test
    void verifyClient() throws Exception {
        LoginRequest request = new LoginRequest("test@email.com", "password");

        ClientDetailsResponse response = new ClientDetailsResponse("test@email.com", 1l, "kwame");


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/login")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //when
        Mockito.doReturn(response).when(authenticationService).verifyClient(request);

        //then
        Mockito.verify(authenticationService, Mockito.times(1)).verifyClient(request);

    }


    @Test
    void doesNotVerifyClientAtWrongEndpoint() throws Exception {
        LoginRequest request = new LoginRequest("test@email.com", "password");

        ClientDetailsResponse response = new ClientDetailsResponse("test@email.com", 1l, "kwame");


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(authenticationService, Mockito.times(0)).verifyClient(request);

    }
}