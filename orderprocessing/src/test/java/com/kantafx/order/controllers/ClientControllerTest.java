package com.kantafx.order.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantafx.order.models.*;
import com.kantafx.order.payloads.*;
import com.kantafx.order.services.ClientService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTest {
    @MockBean
    private ClientService clientService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper om;


    @Test
    void saveClient() throws Exception {
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "kwamesarfo@turntabl.io",
                "Kwame Sarfo",
                "kwame"
        );


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/register")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(1)).saveClient(request);

    }

    @Test
    void doesNotSaveClientAtWrongEndpoint() throws Exception {
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "kwamesarfo@turntabl.io",
                "Kwame Sarfo",
                "kwame"
        );


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(0)).saveClient(request);

    }

    @Test
    void savePortfolio() throws Exception {
        NewPortfolioRequest request = new NewPortfolioRequest(
                "email@email.com",
                "port1",
                100d
        );

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/portfolio")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(1)).createPortfolio(request);
    }

    @Test
    void doesNotSavePortfolioAtWrongEndpoint() throws Exception {
        NewPortfolioRequest request = new NewPortfolioRequest(
                "email@email.com",
                "port1",
                100d
        );

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(0)).createPortfolio(request);
    }

    @Test
    void saveOrder() throws Exception {
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                3,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/order")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(1)).createOrder(request);
    }

    @Test
    void doesNotSaveOrderAtWrongEndpoint() throws Exception {
        ClientOrderRequest request = new ClientOrderRequest(
                TickerLable.AAPL,
                1l,
                3,
                2.50,
                Side.BUY,
                OrderType.MARKET
        );

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(0)).createOrder(request);
    }

    @Test
    void saveStock() throws Exception {
        StockRequest stock = new StockRequest(TickerLable.AAPL, 1L, 20, 9d);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/stock")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(1)).createStock(stock);
    }

    @Test
    void doesNotSaveStockAtWrongEndpoint() throws Exception {
        StockRequest stock = new StockRequest(TickerLable.AAPL, 1L, 20, 9d);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(stock))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();


        //then
        Mockito.verify(clientService, Mockito.times(0)).createStock(stock);
    }

    @Test
    void cancelOrder() throws Exception {
        String exchangeOrderId = "addafe-eresdd";

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/client/order/"+exchangeOrderId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(clientService, Mockito.times(1)).cancelOrder(exchangeOrderId);
    }
    @Test
    void doesNotCancelOrderAtWrongEndpoint() throws Exception {
        String exchangeOrderId = "addafe-eresdd";

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/client/wrongendpoint")
                .accept(MediaType.APPLICATION_JSON)
                .content(exchangeOrderId)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //then
        Mockito.verify(clientService, Mockito.times(0)).cancelOrder(exchangeOrderId);
    }
}