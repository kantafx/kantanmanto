package com.kantafx.market.services;

import com.kantafx.market.dto.TimestampedMarketData;
import com.kantafx.market.models.Exchange;
import com.kantafx.market.repository.ExchangeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@SpringBootTest
@MockBean(MarketDataScheduledService.class)
public class MarketDataServiceTest {

    @MockBean
    ExchangeRepository exchangeRepository;

    @Autowired
    MarketDataService marketDataService;

    @MockBean
    @Qualifier("client")
    WebClient httpClient;

    @MockBean
    @Qualifier("orderBookClient")
    WebClient orderBookClient;

    @Test
    public void getsDataFromExchanges() {
        Exchange exchange = Exchange.builder().id("MAL").endpoint("mal.com").build();
        List<Exchange> exchanges = List.of(exchange);
        Mockito.when(exchangeRepository.findByActive(true)).thenReturn(exchanges);
        var specMock = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
        var headerMock = Mockito.mock(WebClient.RequestHeadersSpec.class);
        var responseMock = Mockito.mock(WebClient.ResponseSpec.class);
        var monoMock = Mockito.mock(Mono.class);
        Mockito.when(httpClient.get()).thenReturn(specMock);
        Mockito.when(specMock.uri("mal.com/pd")).thenReturn(headerMock);
        Mockito.when(headerMock.retrieve()).thenReturn(responseMock);
        Mockito.when(responseMock.bodyToMono(TimestampedMarketData.MarketDataPayload[].class)).thenReturn(monoMock);
        TimestampedMarketData.MarketDataPayload[] data = {new TimestampedMarketData.MarketDataPayload("MSFT", 5000, 1.10, 1.00, 2.0, 2.0, 10000, exchange)};
        Mockito.when(monoMock.block()).thenReturn(data);
        Assertions.assertEquals(marketDataService.getMarketDataFromExchanges(), List.of(data));
    }
}
