package com.kantafx.market.models;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Builder
public class MarketData {
    @Id
    @SequenceGenerator(
            name = "market_id_sequence",
            sequenceName = "market_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "market_id_sequence"
    )
    private Long id;

    @Column(nullable = false)
    private String ticker;

    @Column(nullable = false)
    private Integer sellLimit;

    @Column(nullable = false)
    private Double lastTradedPrice;

    @Column(nullable = false)
    private Double maxPriceShift;

    @Column(nullable = false)
    private Double askPrice;

    @Column(nullable = false)
    private Double bidPrice;

    @Column(nullable = false)
    private Integer buyLimit;

    @Column(nullable = false)
    @CreationTimestamp
    private Timestamp createdAt;

    @ManyToOne
    @JoinColumn(name = "exchange_id", nullable = false)
    private Exchange exchange;
}
