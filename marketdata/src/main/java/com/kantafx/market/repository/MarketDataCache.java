package com.kantafx.market.repository;

import com.kantafx.market.dto.TimestampedMarketData;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MarketDataCache {
    public static TimestampedMarketData data = new TimestampedMarketData(new ArrayList<>(), new Timestamp(new Date().getTime()));

    public static void setData(List<TimestampedMarketData.MarketDataPayload> newData) {
        data = new TimestampedMarketData(newData, new Timestamp(new Date().getTime()));
    }
}
