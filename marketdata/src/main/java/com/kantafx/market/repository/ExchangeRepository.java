package com.kantafx.market.repository;

import com.kantafx.market.models.Exchange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExchangeRepository extends JpaRepository<Exchange, String> {
    List<Exchange> findByActive(Boolean active);

    Optional<Exchange> findByEndpoint(String endpoint);
}
