package com.kantafx.market.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimestampedOrderBook {
    private List<OrderBookPayload> data;
    private Timestamp timestamp;
}
