package com.kantafx.market.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kantafx.market.models.Exchange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TimestampedMarketData {

    private List<MarketDataPayload> marketData;
    private Timestamp timestamp;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class MarketDataPayload {
        @JsonProperty
        private String TICKER;

        @JsonProperty
        private Integer SELL_LIMIT;

        @JsonProperty
        private Double LAST_TRADED_PRICE;

        @JsonProperty
        private Double MAX_PRICE_SHIFT;

        @JsonProperty
        private Double ASK_PRICE;

        @JsonProperty
        private Double BID_PRICE;

        @JsonProperty
        private Integer BUY_LIMIT;
        private Exchange exchange;

        public void setExchange(Exchange exchange) {
            this.exchange = exchange;
        }
    }
}
