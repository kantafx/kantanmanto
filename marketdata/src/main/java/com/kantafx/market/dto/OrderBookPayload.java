package com.kantafx.market.dto;

import com.kantafx.market.models.Exchange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderBookPayload {

    private List<ProductInformation> fullOrderBook;
    private Exchange exchange;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductInformation {
        private String product;
        private int quantity;
        private double price;
        private String side;
        private List<Execution> executions;
        private String orderID;
        private String orderType;
        private int cumulatitiveQuantity;
        private double cumulatitivePrice;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Execution {
        private Timestamp timestamp;
        private double price;
        private int quantity;
    }
}
