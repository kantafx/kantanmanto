package com.kantafx.market;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    public static final String MARKET_DATA_QUEUE = "market-queue";
    public static final String ORDER_BOOK_QUEUE = "order-book-queue";
    public static final String MARKET_DATA_EXCHANGE = "market-data-exchange";
    public static final String ORDER_BOOK_EXCHANGE = "order-book-exchange";
    public static final String MARKET_DATA_ROUTING_KEY = "market_data_routing_key";

    public static final String ORDER_BOOK_ROUTING_KEY = "order_book_routing_key";

    @Bean
    public Queue marketDataQueue() {
        return new Queue(MARKET_DATA_QUEUE);
    }

    @Bean
    public Queue orderBookQueue() {
        return new Queue(ORDER_BOOK_QUEUE);
    }

    @Bean
    public DirectExchange marketDataExchange() {
        return new DirectExchange(MARKET_DATA_EXCHANGE);
    }

    @Bean
    public DirectExchange orderBookExchange() {
        return new DirectExchange(ORDER_BOOK_EXCHANGE);
    }

    @Bean
    public Binding marketDataBinding() {
        return BindingBuilder.bind(marketDataQueue()).to(marketDataExchange()).with(MARKET_DATA_ROUTING_KEY);
    }

    @Bean
    public Binding orderBookBinding() {
        return BindingBuilder.bind(orderBookQueue()).to(orderBookExchange()).with(ORDER_BOOK_ROUTING_KEY);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter());
        return template;
    }
}
