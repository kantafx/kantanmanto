package com.kantafx.market;

import com.kantafx.market.models.Exchange;
import com.kantafx.market.repository.ExchangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@SpringBootApplication
@EnableScheduling
public class MarketDataApplication implements CommandLineRunner {

    @Autowired
    private ExchangeRepository exchangeRepository;


    public static void main(String[] args) {
        SpringApplication.run(MarketDataApplication.class);
    }

    @Override
    public void run(String... args) throws Exception {
        loadExchangeData();
    }

    private void loadExchangeData() {
        Exchange exchange1 = Exchange.builder().id("MAL1").endpoint("https://exchange.matraining.com").active(true).build();
        Exchange exchange2 = Exchange.builder().id("MAL2").endpoint("https://exchange2.matraining.com").active(true).build();
        exchangeRepository.saveAll(List.of(exchange1, exchange2));
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE").allowedOrigins("*");
            }
        };
    }


    @Bean("client")
    public WebClient getWebClient() {
        return WebClient.create();
    }

    @Bean("orderBookClient")
    public WebClient orderBookClient() {
        final int size = 16 * 1024 * 1024;
        final ExchangeStrategies strategies = ExchangeStrategies.builder().codecs(codes -> codes.defaultCodecs().maxInMemorySize(size)).build();
        return WebClient.builder().exchangeStrategies(strategies).build();
    }
}
