package com.kantafx.market.controllers;

import com.kantafx.market.exceptions.ResourceAlreadyExistsException;
import com.kantafx.market.exceptions.ResourceNotFoundException;
import com.kantafx.market.models.Exchange;
import com.kantafx.market.services.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class ExchangeController {

    @Autowired
    private ExchangeService exchangeService;

    @GetMapping("/exchanges/{id}")
    public ResponseEntity<?> getExchangeById(@PathVariable String id) {
        Optional<Exchange> possibleExchange = exchangeService.getExchangeById(id);
        if(possibleExchange.isEmpty()) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(possibleExchange.get());
    }
    @GetMapping("/exchanges/{id}/endpoint")
    public ResponseEntity<?> findExchangeEndpointById(@PathVariable(name = "id") String id) {
        try {
            String endpoint = exchangeService.findExchangeEndpointById(id);
            return ResponseEntity.ok(endpoint);
        }catch (ResourceNotFoundException resourceNotFoundException)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/exchanges")
    public ResponseEntity<?> createExchange(@RequestBody Exchange exchange) {
        try {
            Exchange addedExchange = exchangeService.addExchange(exchange);
            return ResponseEntity.ok(addedExchange);
        } catch (ResourceAlreadyExistsException exception) {
            return ResponseEntity.badRequest().body(Map.of("message", "Resource already exists"));
        }
    }

    @GetMapping("/exchanges")
    public ResponseEntity<?> getAllExchanges() {
        List<Exchange> exchanges = exchangeService.fetchAllExchanges();
        return ResponseEntity.ok(exchanges);
    }

    @PutMapping("/exchanges/{id}")
    public ResponseEntity<?> modifyExchange(@PathVariable String id, @RequestBody Exchange exchange) {
        try {
            Exchange updatedExchange = exchangeService.updateExchange(id, exchange);
            return ResponseEntity.ok(updatedExchange);
        } catch (ResourceNotFoundException exception) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/exchanges/{id}")
    public ResponseEntity<?> deleteExchange(@PathVariable String id) {
        try {
            exchangeService.deleteExchange(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException exception) {
            return ResponseEntity.notFound().build();
        }
    }
}
