package com.kantafx.market.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantafx.market.dto.TimestampedMarketData;
import com.kantafx.market.repository.MarketDataCache;
import com.kantafx.market.services.MarketDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MarketDataController {

    @Autowired
    private MarketDataService marketDataService;

    @GetMapping("/availableData")
    public ResponseEntity<?> getAllAvailableMarketData() {
        return ResponseEntity.ok(marketDataService.getMarketDataFromExchanges());
    }

    @PostMapping("/marketOrders")
    public ResponseEntity<?> getMarketOrders() {
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/stream-flux/{ticker}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> streamFlux(@PathVariable String ticker) {
        return Flux.interval(Duration.ofSeconds(2)).map(sequence -> {
            List<TimestampedMarketData.MarketDataPayload> data = MarketDataCache.data.getMarketData().stream().filter(d -> d.getTICKER().equals(ticker)).toList();
            Map<String, List<Double>> payload = new HashMap<>();
            for (TimestampedMarketData.MarketDataPayload d : data) {
                if (payload.containsKey(d.getTICKER())) {
                    List<Double> currentPrices = payload.get(d.getTICKER());
                    currentPrices.add(d.getLAST_TRADED_PRICE());
                    payload.put(d.getTICKER(), currentPrices);
                } else {
                    payload.put(d.getTICKER(), new ArrayList<>(List.of(d.getLAST_TRADED_PRICE())));
                }
            }
            try {
                return new ObjectMapper().writeValueAsString(payload);
            } catch (JsonProcessingException e) {
                return "";
            }
        });
    }
}
