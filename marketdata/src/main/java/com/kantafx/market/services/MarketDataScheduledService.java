package com.kantafx.market.services;

import com.kantafx.market.RabbitConfig;
import com.kantafx.market.dto.OrderBookPayload;
import com.kantafx.market.dto.TimestampedMarketData;
import com.kantafx.market.dto.TimestampedOrderBook;
import com.kantafx.market.repository.MarketDataCache;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class MarketDataScheduledService {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    MarketDataService marketDataService;

    @Scheduled(fixedDelay = 200)
    public void publishMarketData() {
        List<TimestampedMarketData.MarketDataPayload> data = marketDataService.getMarketDataFromExchanges();
        template.convertAndSend(RabbitConfig.MARKET_DATA_EXCHANGE, RabbitConfig.MARKET_DATA_ROUTING_KEY, new TimestampedMarketData(data, new Timestamp(new Date().getTime())));
        System.out.println("Market Data sent");
    }

    @Scheduled(fixedDelay = 200)
    public void publishOrderBooks() {
        List<OrderBookPayload> data = marketDataService.getOrderBooksFromExchanges();
        template.convertAndSend(RabbitConfig.ORDER_BOOK_EXCHANGE, RabbitConfig.ORDER_BOOK_ROUTING_KEY, new TimestampedOrderBook(data, new Timestamp(new Date().getTime())));
        System.out.println("Order book sent");
    }

    @Scheduled(fixedDelay = 500)
    public void getOrderData() {
        List<TimestampedMarketData.MarketDataPayload> data = marketDataService.getMarketDataFromExchanges();
        MarketDataCache.setData(data);
    }
}
