package com.kantafx.market.services;

import com.kantafx.market.exceptions.ResourceAlreadyExistsException;
import com.kantafx.market.exceptions.ResourceNotFoundException;
import com.kantafx.market.models.Exchange;
import com.kantafx.market.repository.ExchangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class ExchangeService {

    @Autowired
    private ExchangeRepository exchangeRepository;

    public Optional<Exchange> getExchangeById(String id) {
        return exchangeRepository.findById(id);
    }

    public List<Exchange> fetchAllExchanges() {
        return exchangeRepository.findAll();
    }

    public Exchange addExchange(Exchange exchange) throws ResourceAlreadyExistsException {
        Optional<Exchange> exchangePresent = exchangeRepository.findById(exchange.getId());

        if(exchangePresent.isPresent()) throw new ResourceAlreadyExistsException();

        Exchange newExchange = exchangeRepository.save(Exchange.builder()
                .endpoint(exchange.getEndpoint())
                .id(exchange.getId())
                .active(exchange.getActive())
                .build());

        MarketDataService.ExchangeCache.lastModifiedExchanges = new Timestamp(new Date().getTime());
        return newExchange;
    }

    public Exchange updateExchange(String exchangeId, Exchange exchange) throws ResourceNotFoundException {
        Optional<Exchange> exchangePresent = getExchangeById(exchangeId);

        if(exchangePresent.isEmpty()) throw new ResourceNotFoundException();

        Exchange exchangeInDb = exchangePresent.get();

        exchangeInDb.setEndpoint(exchange.getEndpoint());
        exchangeInDb.setId(exchange.getId());
        exchangeInDb.setActive(exchange.getActive());
        exchangeInDb.setUpdatedAt(new Timestamp(new Date().getTime()));

        Exchange updatedExchange = exchangeRepository.save(exchangeInDb);
        MarketDataService.ExchangeCache.lastModifiedExchanges = new Timestamp(new Date().getTime());

        return updatedExchange;
    }

    public void deleteExchange(String id) throws ResourceNotFoundException {
        Optional<Exchange> exchangePresent = getExchangeById(id);

        if(exchangePresent.isEmpty()) throw new ResourceNotFoundException();

        exchangeRepository.deleteById(id);

        MarketDataService.ExchangeCache.lastModifiedExchanges = new Timestamp(new Date().getTime());
    }

    public String findExchangeEndpointById(String id) throws ResourceNotFoundException {
        Optional<Exchange> exchangePresent = getExchangeById(id);

        if(exchangePresent.isEmpty()) throw new ResourceNotFoundException();

        return exchangePresent.get().getEndpoint();
    }
}