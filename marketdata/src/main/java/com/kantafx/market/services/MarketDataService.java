package com.kantafx.market.services;

import com.kantafx.market.dto.OrderBookPayload;
import com.kantafx.market.dto.TimestampedMarketData;
import com.kantafx.market.models.Exchange;
import com.kantafx.market.repository.ExchangeRepository;
import com.kantafx.market.repository.MarketDataRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MarketDataService {

    @Autowired
    private WebClient client;

    @Autowired
    private WebClient orderBookClient;

    @Autowired
    private MarketDataRepository marketDataRepository;

    @Autowired
    private ExchangeRepository exchangeRepository;


    private List<Exchange> loadExchangeFromCacheOrDB() {
        if(ExchangeCache.lastCheckData == null || ExchangeCache.lastModifiedExchanges != null &&  ExchangeCache.lastCheckData.before(ExchangeCache.lastModifiedExchanges)) {
            List<Exchange> exchanges = exchangeRepository.findByActive(true);
            ExchangeCache.exchanges = exchanges;
            ExchangeCache.lastCheckData = new Timestamp(new Date().getTime());
            return exchanges;
        } else {
            return ExchangeCache.exchanges;
        }
    }

    public List<TimestampedMarketData.MarketDataPayload> getMarketDataFromExchanges() {
        List<Exchange> exchanges = loadExchangeFromCacheOrDB();
        List<TimestampedMarketData.MarketDataPayload> marketDataPayload = new ArrayList<>();
        for(Exchange exchange : exchanges) {
            String subscriptionEndpoint = String.format("%s/pd",exchange.getEndpoint());
            WebClient.ResponseSpec response = client.get().uri(subscriptionEndpoint).retrieve();
            TimestampedMarketData.MarketDataPayload[] data = response.bodyToMono(TimestampedMarketData.MarketDataPayload[].class).block();
            if(data != null) {
                for(TimestampedMarketData.MarketDataPayload payload : data) {
                    payload.setExchange(exchange);
                }
                marketDataPayload.addAll(List.of(data));
            }
        }
        return marketDataPayload;
    }


    public List<OrderBookPayload> getOrderBooksFromExchanges() {
        List<Exchange> exchanges = loadExchangeFromCacheOrDB();
        List<OrderBookPayload> orderBookPayload = new ArrayList<>();
        for (Exchange exchange : exchanges) {
            String orderBookEndpoint = String.format("%s/orderbook", exchange.getEndpoint());
            WebClient.ResponseSpec response = orderBookClient.get().uri(orderBookEndpoint).retrieve();
            OrderBookPayload[] data = response.bodyToMono(OrderBookPayload[].class).block();
            if(data!=null) {
                for(OrderBookPayload payload : data) {
                    payload.setExchange(exchange);
                }
                orderBookPayload.addAll(List.of(data));
            }
        }
    return orderBookPayload;
    }

    static class ExchangeCache {
        public static Timestamp lastCheckData;

        public static Timestamp lastModifiedExchanges;

        public static List<Exchange> exchanges = new ArrayList<>();
    }
}
